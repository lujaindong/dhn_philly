#-*- coding:utf-8 -*-
import tensorflow as tf
import util as util
import numpy as np
dir(tf.contrib)

#功能：读取样本，计算user embedding
#model_path:模型目录
#infer_path:待计算user embedding文件的目录
#param_path:参数文件的目录
def infer(model_path, infer_path, res_path, param_path):
    #需要进行计算user embedding文件的文件名
    INFER_FILE_NAME = 'user_embedding_new_format'
    #存user embedding文件结果的名字
    RES_FILE_NAME = 'infer_embed_res'
    BATCH_SIZE = 32

    infer_file_name =  infer_path + '/' + INFER_FILE_NAME
    uid_list = util.get_uid(infer_file_name)
    print('need to infer user embedding sample num:', len(uid_list))
    res_file_name = res_path + '/' + RES_FILE_NAME
    res_writer = open(res_file_name, 'w')

    hparams = {}
    hparams['batch_size'] = BATCH_SIZE
    #load param for param dir
    util.parser_param_from_cosmos(param_path, hparams)

    sess = tf.Session()
    meta_graph_def = tf.saved_model.loader.load(sess, ['tag_string'], model_path)
    # 将code里面的几个迭代器给取出来.
    T2U_pair_feat_batch = sess.graph.get_tensor_by_name('T2U_pair_feat_batch:0')
    T2U_feat_batch = sess.graph.get_tensor_by_name('T2U_feat_batch:0')
    T1U_pair_feat_batch_indices = sess.graph.get_tensor_by_name('T1U_pair_feat_batch_indices:0')
    T1U_pair_feat_batch_val = sess.graph.get_tensor_by_name('T1U_pair_feat_batch_val:0')
    T1U_pair_feat_batch_weights = sess.graph.get_tensor_by_name('T1U_pair_feat_batch_weights:0')
    T1U_pair_feat_batch_shape = sess.graph.get_tensor_by_name('T1U_pair_feat_batch_shape:0')
    T0U_unique_feat_batch_indices = sess.graph.get_tensor_by_name('T0U_unique_feat_batch_indices:0')
    T0U_unique_feat_batch_val = sess.graph.get_tensor_by_name('T0U_unique_feat_batch_val:0')
    T0U_unique_feat_batch_weights = sess.graph.get_tensor_by_name('T0U_unique_feat_batch_weights:0')
    T0U_unique_feat_batch_shape = sess.graph.get_tensor_by_name('T0U_unique_feat_batch_shape:0')
    user_embedding = sess.graph.get_tensor_by_name('DhnModel/user_embedding:0')

    uid_idx = 0
    for (labels_v, T2U_feat_batch_v, T2U_pair_feat_batch_v, T1U_pair_feat_batch_v, T0U_unique_feat_batch_v) in util.load_batch_data_from_file(infer_file_name, hparams):
        input_in_sp = util.convert_data(labels_v, T2U_feat_batch_v, T2U_pair_feat_batch_v, T1U_pair_feat_batch_v, T0U_unique_feat_batch_v, hparams)
        user_embed = sess.run(user_embedding, feed_dict={
            T2U_pair_feat_batch: input_in_sp['T2U_pair_feat_batch'],
            T2U_feat_batch: input_in_sp['T2U_feat_batch'],
            T1U_pair_feat_batch_indices: input_in_sp['T1U_pair_feat_batch_indices'],
            T1U_pair_feat_batch_val: input_in_sp['T1U_pair_feat_batch_val'],
            T1U_pair_feat_batch_weights: input_in_sp['T1U_pair_feat_batch_weights'],
            T1U_pair_feat_batch_shape: input_in_sp['T1U_pair_feat_batch_shape'],
            T0U_unique_feat_batch_indices: input_in_sp['T0U_unique_feat_batch_indices'],
            T0U_unique_feat_batch_val: input_in_sp['T0U_unique_feat_batch_val'],
            T0U_unique_feat_batch_weights: input_in_sp['T0U_unique_feat_batch_weights'],
            T0U_unique_feat_batch_shape: input_in_sp['T0U_unique_feat_batch_shape']
        })
        for one_embed in user_embed:
            uid = uid_list[uid_idx]
            uid_idx += 1
            line = uid + ':' + ','.join([str(val) for val in one_embed]) + '\n'
            res_writer.write(line)
    res_writer.close()

if __name__=="__main__":
    model_path = './model/epoch_1'
    infer_path = './data'
    res_path = './res'
    param_path = './params'
    infer(model_path, infer_path, res_path, param_path)
