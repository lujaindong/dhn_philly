#-*- coding:utf-8 -*-
import os
import numpy as np

def get_uid(filename):
    uid_list = []
    for line in open(filename, encoding='utf8', mode='r'):
        if line.strip() is '':
            continue
        items = line.strip().split('')
        uid = items[-3]
        uid_list.append(uid)
    return uid_list

def parser_param_from_cosmos(rootDir, config):
    files = os.listdir(rootDir)
    #parser T0I_unique_field_num
    T0I_unique_field_num_file = 'T0I_field_stat.tsv'
    if T0I_unique_field_num_file in files:
        T0I_unique_field_num = int(open(rootDir+'/'+T0I_unique_field_num_file).readlines()[0])
        if T0I_unique_field_num == 0:
            T0I_unique_field_num = 1
        print(T0I_unique_field_num_file, T0I_unique_field_num)

    # parser T0I_unique_feat_num
    T0I_unique_feat_num_file = 'T0I_feature_stat.tsv'
    if T0I_unique_feat_num_file in files:
        T0I_unique_feat_num = int(open(rootDir + '/' +T0I_unique_feat_num_file).readlines()[0])
        if T0I_unique_feat_num == 0:
            T0I_unique_feat_num = 2
        else:
            T0I_unique_feat_num += 1
        print(T0I_unique_feat_num_file, T0I_unique_feat_num)

    # parser T0U_unique_field_num
    T0U_unique_field_num_file = 'T0U_field_stat.tsv'
    if T0U_unique_field_num_file in files:
        T0U_unique_field_num = int(open(rootDir + '/' +T0U_unique_field_num_file).readlines()[0])
        if T0U_unique_field_num == 0:
            T0U_unique_field_num = 1
        print(T0U_unique_field_num_file, T0U_unique_field_num)

    # parser T0U_unique_feat_num
    T0U_unique_feat_num_file = 'T0U_feature_stat.tsv'
    if T0U_unique_feat_num_file in files:
        T0U_unique_feat_num = int(open(rootDir + '/' +T0U_unique_feat_num_file).readlines()[0])
        if T0U_unique_feat_num == 0:
            T0U_unique_feat_num = 2
        else:
            T0U_unique_feat_num += 1
        print(T0U_unique_feat_num_file, T0U_unique_feat_num)

    # parser T1_pair_field_num
    T1_pair_field_num_file = 'T1_field_stat.tsv'
    if T1_pair_field_num_file in files:
        T1_pair_field_num = int(open(rootDir + '/' +T1_pair_field_num_file).readlines()[0])
        if T1_pair_field_num == 0:
            T1_pair_field_num = 1
        print(T1_pair_field_num_file, T1_pair_field_num)

    # parser T2_pair_feat_num
    T1_pair_feat_num_file = 'T1_feature_stat.tsv'
    if T1_pair_feat_num_file in files:
        T1_pair_feat_num = int(open(rootDir + '/' +T1_pair_feat_num_file).readlines()[0])
        if T1_pair_feat_num == 0:
            T1_pair_feat_num = 2
        else:
            T1_pair_feat_num += 1
        print(T1_pair_feat_num_file, T1_pair_feat_num)

    # parser T2U_feat_len
    T2U_feat_len_file = 'T2U_feat_stat.txt'
    if T2U_feat_len_file in files:
        T2U_feat_len = int(open(rootDir + '/' +T2U_feat_len_file).readlines()[0])
    else:
        T2U_feat_len = 1
    print(T2U_feat_len_file, T2U_feat_len)

    # parser T2I_feat_len
    T2I_feat_len_file = 'T2I_feat_stat.txt'
    if T2I_feat_len_file in files:
        T2I_feat_len = int(open(rootDir + '/' +T2I_feat_len_file).readlines()[0])
    else:
        T2I_feat_len = 1
    print(T2I_feat_len_file, T2I_feat_len)

    #T2_pair_feat_num
    T2_pair_feat_file = 'T2_pair_feat_stat.txt'
    if T2_pair_feat_file in files:
        T2_pair_feat_num = int(open(rootDir + '/' +T2_pair_feat_file).readlines()[0])
    else:
        T2_pair_feat_num = 1
    print(T2_pair_feat_file, T2_pair_feat_num)
    config['T2U_feat_len'] = T2U_feat_len
    config['T2I_feat_len'] = T2I_feat_len
    config['T2_pair_feat_num'] = T2_pair_feat_num
    config['T1_pair_field_num'] = T1_pair_field_num
    config['T1_pair_feat_num'] = T1_pair_feat_num

    config['T0U_unique_field_num'] = T0U_unique_field_num
    config['T0U_unique_feat_num'] = T0U_unique_feat_num
    config['T0I_unique_field_num'] = T0I_unique_field_num
    config['T0I_unique_feat_num'] = T0I_unique_feat_num


# field index start by 1, feat index start by 1
def load_batch_data_from_file(file, hparams):
    batch_size = hparams['batch_size']
    labels = []
    T2U_pair_feat_batch = []
    T2U_feat_batch = []
    T1U_pair_feat_batch = []
    T0U_unique_feat_batch = []
    cnt = 0
    with open(file, encoding='utf8') as f:
        for line in f:
            if line.strip() is '':
                continue
            cols = line.strip().split('\x03')
            if len(cols) < 9:
                continue
            label = cols[0]
            if float(label) > 0:
                label = 1
            else:
                label = 0
            # process T2U_feat
            if cols[1].strip() is '':
                T2U_feat = [0.0]
            else:
                T2U_feat = cols[1].strip().split(',')
                T2U_feat = [float(val) for val in T2U_feat]
            # process T2U_pair_feat
            if cols[3].strip() is '':
                feat_embedding_user_cascade = [0.0]
            else:
                pair_feat_items = cols[3].strip().split(':')
                if len(pair_feat_items) > 0:
                    feat_embedding_user_cascade = []
                    for i in range(len(pair_feat_items)):
                        if i % 2 == 0:
                            pass
                        else:
                            feat_embedding = [float(val) for val in pair_feat_items[i].strip().split(',')]
                            feat_embedding_user_cascade.extend(feat_embedding)
            # process T1U_pair_feat
            curr_T1U_pair_feat = []
            for field in range(hparams['T1_pair_field_num']):
                curr_T1U_pair_feat.append([field, 0, 0])
            if cols[5].strip() is '':
                pass
            else:
                terms = cols[5].strip().split(',')
                for word in terms:
                    tokens = word.strip().split(':')
                    curr_T1U_pair_feat.append([int(tokens[0]) - 1, int(tokens[1]), float(tokens[2])])

            # process T0U_unique_feat
            curr_T0U_unique_feat = []
            # 保证每个field都有一个feat, 因此对每个field都填充一个field_index:0:0
            for field in range(hparams['T0U_unique_field_num']):
                curr_T0U_unique_feat.append([field, 0, 0])
            if cols[7].strip() is '':
                pass
            else:
                terms = cols[7].strip().split(',')
                for word in terms:
                    tokens = word.strip().split(':')
                    curr_T0U_unique_feat.append([int(tokens[0]) - 1, int(tokens[1]), float(tokens[2])])
            cnt += 1
            labels.append([label])
            T2U_pair_feat_batch.append(feat_embedding_user_cascade)
            T2U_feat_batch.append(T2U_feat)
            T1U_pair_feat_batch.append(curr_T1U_pair_feat)
            T0U_unique_feat_batch.append(curr_T0U_unique_feat)
            if cnt == batch_size:
                yield labels, T2U_feat_batch, T2U_pair_feat_batch, T1U_pair_feat_batch, \
                      T0U_unique_feat_batch
                labels = []
                T2U_feat_batch = []
                T2U_pair_feat_batch = []
                T1U_pair_feat_batch = []
                T0U_unique_feat_batch = []
                cnt = 0
        if cnt > 0:
            yield labels, T2U_feat_batch, T2U_pair_feat_batch, T1U_pair_feat_batch, \
                  T0U_unique_feat_batch


def convert_data(labels, T2U_feat_batch, T2U_pair_feat_batch, T1U_pair_feat_batch, \
                 T0U_unique_feat_batch, hparams):
    batch_size = hparams['batch_size']
    instance_cnt = len(labels)

    T1U_pair_feat_batch_indices = []
    T1U_pair_feat_batch_val = []
    T1U_pair_feat_batch_weights = []
    T1U_pair_feat_batch_shape = [instance_cnt * hparams['T1_pair_field_num'], -1]
    for i in range(instance_cnt):
        m = len(T1U_pair_feat_batch[i])
        dnn_feat_dic = {}
        for j in range(m):
            if T1U_pair_feat_batch[i][j][0] not in dnn_feat_dic:
                dnn_feat_dic[T1U_pair_feat_batch[i][j][0]] = 0
            else:
                dnn_feat_dic[T1U_pair_feat_batch[i][j][0]] += 1
            T1U_pair_feat_batch_indices.append([i * hparams['T1_pair_field_num'] + T1U_pair_feat_batch[i][j][0], \
                                                dnn_feat_dic[T1U_pair_feat_batch[i][j][0]]])
            T1U_pair_feat_batch_val.append(T1U_pair_feat_batch[i][j][1])
            T1U_pair_feat_batch_weights.append(T1U_pair_feat_batch[i][j][2])
            if T1U_pair_feat_batch_shape[1] < dnn_feat_dic[T1U_pair_feat_batch[i][j][0]]:
                T1U_pair_feat_batch_shape[1] = dnn_feat_dic[T1U_pair_feat_batch[i][j][0]]
    T1U_pair_feat_batch_shape[1] += 1

    T0U_unique_feat_batch_indices = []
    T0U_unique_feat_batch_val = []
    T0U_unique_feat_batch_weights = []
    T0U_unique_feat_batch_shape = [instance_cnt * hparams['T0U_unique_field_num'], -1]
    for i in range(instance_cnt):
        m = len(T0U_unique_feat_batch[i])
        dnn_feat_dic = {}
        for j in range(m):
            if T0U_unique_feat_batch[i][j][0] not in dnn_feat_dic:
                dnn_feat_dic[T0U_unique_feat_batch[i][j][0]] = 0
            else:
                dnn_feat_dic[T0U_unique_feat_batch[i][j][0]] += 1
            T0U_unique_feat_batch_indices.append([i * hparams['T0U_unique_field_num'] + T0U_unique_feat_batch[i][j][0], \
                                                  dnn_feat_dic[T0U_unique_feat_batch[i][j][0]]])
            T0U_unique_feat_batch_val.append(T0U_unique_feat_batch[i][j][1])
            T0U_unique_feat_batch_weights.append(T0U_unique_feat_batch[i][j][2])
            if T0U_unique_feat_batch_shape[1] < dnn_feat_dic[T0U_unique_feat_batch[i][j][0]]:
                T0U_unique_feat_batch_shape[1] = dnn_feat_dic[T0U_unique_feat_batch[i][j][0]]
    T0U_unique_feat_batch_shape[1] += 1

    res = {}
    res['labels'] = np.asarray(labels, dtype=np.float32)
    res['T2U_feat_batch'] = np.asarray(T2U_feat_batch, dtype=np.float32)
    res['T2U_pair_feat_batch'] = np.asarray(T2U_pair_feat_batch, dtype=np.float32)

    sorted_T1U_indices = sorted(range(len(T1U_pair_feat_batch_indices)),
                                key=lambda k: (T1U_pair_feat_batch_indices[k][0], \
                                               T1U_pair_feat_batch_indices[k][1]))
    res['T1U_pair_feat_batch_indices'] = np.asarray(T1U_pair_feat_batch_indices, dtype=np.int64)[
        sorted_T1U_indices]
    res['T1U_pair_feat_batch_val'] = np.asarray(T1U_pair_feat_batch_val, dtype=np.int64)[sorted_T1U_indices]
    res['T1U_pair_feat_batch_weights'] = np.asarray(T1U_pair_feat_batch_weights, dtype=np.float32)[
        sorted_T1U_indices]
    res['T1U_pair_feat_batch_shape'] = np.asarray(T1U_pair_feat_batch_shape, dtype=np.int64)

    sorted_T0U_indices = sorted(range(len(T0U_unique_feat_batch_indices)),
                                key=lambda k: (T0U_unique_feat_batch_indices[k][0], \
                                               T0U_unique_feat_batch_indices[k][1]))
    res['T0U_unique_feat_batch_indices'] = np.asarray(T0U_unique_feat_batch_indices, dtype=np.int64)[
        sorted_T0U_indices]
    res['T0U_unique_feat_batch_val'] = np.asarray(T0U_unique_feat_batch_val, dtype=np.int64)[sorted_T0U_indices]
    res['T0U_unique_feat_batch_weights'] = np.asarray(T0U_unique_feat_batch_weights, dtype=np.float32)[
        sorted_T0U_indices]
    res['T0U_unique_feat_batch_shape'] = np.asarray(T0U_unique_feat_batch_shape, dtype=np.int64)

    return res