import sys
from collections import defaultdict

def load_dict(filename):
    feat_idx = {}
    with open(filename, mode='r', encoding='utf-8') as f:
        for line in f:
            tmps = line.strip().split(',')
            if len(tmps) < 2:
                continue
            feat_idx[tmps[1]] = tmps[0]
    return feat_idx

def convert_T1_pair(feats, field_hash, feat_hash):
    convert_feats = []
    for feat in feats:
        items = feat.strip().split(':')
        field = items[1]
        feat = items[1]+':'+items[2]
        val = items[3]
        if feat not in feat_hash:
            continue
        new_feat = str(field_hash[field]) +':' +str(feat_hash[feat]) + ':' + str(val)
        convert_feats.append(new_feat)
    if len(convert_feats) == 0:
        convert_feats.append('1:1:0')
    return ','.join(convert_feats)

def convert_T0U_unique(feats, field_hash, feat_hash):
    convert_feats = []
    for feat in feats:
        items = feat.strip().split(':')
        field = items[1]
        feat = items[1]+':'+items[2]
        val = items[3]
        #if feat not in feat_hash, drop the feat
        if feat not in feat_hash:
            continue
        new_feat = str(field_hash[field]) +':' +str(feat_hash[feat]) + ':' + str(val)
        convert_feats.append(new_feat)
    if len(convert_feats) == 0:
        convert_feats.append('1:1:0')
    return ','.join(convert_feats)

def convert_T0I_unique(feats, field_hash, feat_hash):
    convert_feats = []
    for feat in feats:
        items = feat.strip().split(':')
        field = items[1]
        feat = items[1]+':'+items[2]
        val = items[3]
        if feat not in feat_hash:
            continue
        new_feat = str(field_hash[field]) +':' +str(feat_hash[feat]) + ':' + str(val)
        convert_feats.append(new_feat)
    if len(convert_feats) == 0:
        convert_feats.append('1:1:0')
    return ','.join(convert_feats)

def conver_feat(line):
    tmps = line.strip().split(',')
    if len(tmps) < 5:
        return None
    label = tmps[0]
    uid = tmps[1]
    iid = tmps[2]
    date = tmps[3]
    featStr = tmps[4]
    conver_feats = []
    feats = featStr.strip().split('\x03')
    key_val = defaultdict(lambda: [])
    for item in feats:
        key = item.strip().split(':')[0]
        key_val[key].append(item)
    conver_feats = []
    if 'T2U_feat' not in key_val:
        T2U_feat = ''
    else:
        pass
    if 'T2I_feat' not in key_val:
        T2I_feat = ''
    else:
        pass
    if 'T2U_pair_feat' not in key_val:
        T2U_pair_feat = ''
    else:
        pass
    if 'T2I_pair_feat' not in key_val:
        T2I_pair_feat = ''
    else:
        pass
    if 'T1U_pair_feat' not in key_val:
        T1U_pair_feat = '1:1:0'
    else:
        T1U_pair_feat = convert_T1_pair(key_val['T1U_pair_feat'], T1_pair_field_hash, T1_pair_feat_hash)
    if 'T1I_pair_feat' not in key_val:
        T1I_pair_feat = '1:1:0'
    else:
        T1I_pair_feat = convert_T1_pair(key_val['T1I_pair_feat'], T1_pair_field_hash, T1_pair_feat_hash)
    if 'T0U_unique_feat' not in key_val:
        T0U_unique_feat = '1:1:0'
    else:
        T0U_unique_feat = convert_T0U_unique(key_val['T0U_unique_feat'], T0U_field_hash, T0U_feat_hash)
    if 'T0I_unique_feat' not in key_val:
        T0I_unique_feat = '1:1:0'
    else:
        T0I_unique_feat = convert_T0I_unique(key_val['T0I_unique_feat'], T0I_field_hash, T0I_feat_hash)
    conver_feats.append(label)
    conver_feats.append(T2U_feat)
    conver_feats.append(T2I_feat)
    conver_feats.append(T2U_pair_feat)
    conver_feats.append(T2I_pair_feat)
    conver_feats.append(T1U_pair_feat)
    conver_feats.append(T1I_pair_feat)
    conver_feats.append(T0U_unique_feat)
    conver_feats.append(T0I_unique_feat)
    conver_feats.append(uid)
    conver_feats.append(iid)
    conver_feats.append(date)
    return '\x03'.join(conver_feats)+'\n'

def convert(input_path, dict_path, res_path):
    #输入文件的名字
    INPUT_NAME = 'data.csv'
    #输出文件的名字
    CONVERTED_NAME = 'res.csv'
    res_file = res_path + '/' + CONVERTED_NAME
    out = open(res_file, mode='w', encoding='utf-8')
    input_file_name = input_path + '/' + INPUT_NAME
    with open(input_file_name, mode='r', encoding='utf-8') as f:
        for line in f:
            converte_feat = conver_feat(line)
            if converte_feat is None:
                continue
            else:
                out.write(converte_feat)

if __name__ == "__main__":
    #dict_path：各种映射表的路径
    #input_path: 输入数据的路径
    #res_path: 结果文件的路径
    dict_path = './dict'
    input_path = './data'
    res_path = './res'

    FILE_DICT_NAME = 'T1_field_map.tsv'
    FEAT_DICT_NAME = 'T1_feature_map.tsv'
    field_hash_file = dict_path + '/' + FILE_DICT_NAME
    T1_pair_field_hash = load_dict(field_hash_file)
    feat_hash_file = dict_path + '/' + FEAT_DICT_NAME
    T1_pair_feat_hash = load_dict(feat_hash_file)

    FILE_DICT_NAME = 'T0U_field_map.tsv'
    FEAT_DICT_NAME = 'T0U_feature_map.tsv'
    field_hash_file = dict_path + '/' + FILE_DICT_NAME
    T0U_field_hash = load_dict(field_hash_file)
    feat_hash_file = dict_path + '/' + FEAT_DICT_NAME
    T0U_feat_hash = load_dict(feat_hash_file)

    FILE_DICT_NAME = 'T0I_field_map.tsv'
    FEAT_DICT_NAME = 'T0I_feature_map.tsv'
    field_hash_file = dict_path + '/' + FILE_DICT_NAME
    T0I_field_hash = load_dict(field_hash_file)
    feat_hash_file = dict_path + '/' + FEAT_DICT_NAME
    T0I_feat_hash = load_dict(feat_hash_file)

    convert(input_path, dict_path, res_path)

