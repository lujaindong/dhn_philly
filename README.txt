说明：
DHN代码的project:
1、这个code是在philly上能够成功运行的
2、运行方式:
python main.py --input-training-data-path D:\dhn_final_philly\data\ --output-model-path D:\dhn_final_philly\data\  --layer_sizes 100#100 --activation sigmoid#sigmoid --dim 10 --init_value 0.01 --learning_rate 0.01 --epochs 2 --batch_size 4096 --eval_flag 0 --user_embed_flag 1 --regular 0.0 --show_step 100 --save_epoch 1


infer代码的project:
infer_code主要是convert和infer函数
1、convert.py:将数据转换成特征文件
2、infer.py:从模型infer出最后的embedding
