"""define deep search Network"""
import math
import tensorflow as tf
import numpy as np
from base_model import BaseModel

__all__ = ["DhnModel"]


class DhnModel(BaseModel):
    """define deep search Network"""

    def _build_graph(self, hparams):
        with tf.variable_scope("DhnModel") as scope:
            logit = self._build_dhn(hparams)
            return logit

    def _build_dhn(self, hparams):
        user_embed_input, user_embed_input_dim = self._build_user_embedding(hparams)
        item_embed_input, item_embed_input_dim = self._build_item_embedding(hparams)
        with tf.variable_scope("DnnModelForUserEmbedding", initializer=self.initializer) as scope:
            user_embed_output, user_embed_size = self._build_dnn(user_embed_input, user_embed_input_dim,
                                                                 hparams.layer_sizes, hparams.activation)

        with tf.variable_scope("DnnModelForItemEmbedding", initializer=self.initializer) as scope:
            item_embed_output, item_embed_size = self._build_dnn(item_embed_input, item_embed_input_dim,
                                                                 hparams.layer_sizes, hparams.activation)

        # cosine similarity score
        norm_user_embedding = tf.nn.l2_normalize(user_embed_output, dim=1, name='user_embedding')
        norm_item_embedding = tf.nn.l2_normalize(item_embed_output, dim=1, name='item_embedding')
        # print('norm_user_embedding:', norm_user_embedding)
        # print('norm_item_embedding:', norm_item_embedding)
        cos_similarity = tf.multiply(norm_user_embedding, norm_item_embedding)
        cos_similarity = tf.reduce_sum(cos_similarity, axis=1, keep_dims=True)
        return cos_similarity

    def _build_dnn(self, nn_input, last_layer_size, layer_sizes, activations):
        layer_idx = 0
        hidden_nn_layers = []
        hidden_nn_layers.append(nn_input)
        with tf.variable_scope("nn_part", initializer=self.initializer) as scope:
            for idx, layer_size in enumerate(layer_sizes):
                curr_w_nn_layer = tf.get_variable(name='w_nn_layer' + str(layer_idx),
                                                  shape=[last_layer_size, layer_size],
                                                  dtype=tf.float32)
                curr_b_nn_layer = tf.get_variable(name='b_nn_layer' + str(layer_idx),
                                                  shape=[layer_size],
                                                  dtype=tf.float32)
                curr_hidden_nn_layer = tf.nn.xw_plus_b(hidden_nn_layers[layer_idx],
                                                       curr_w_nn_layer,
                                                       curr_b_nn_layer)
                scope = "nn_part" + str(idx)
                activation = activations[idx]
                curr_hidden_nn_layer = self._active_layer(logit=curr_hidden_nn_layer,
                                                          scope=scope,
                                                          activation=activation)
                hidden_nn_layers.append(curr_hidden_nn_layer)
                layer_idx += 1
                last_layer_size = layer_size
                self.layer_params.append(curr_w_nn_layer)
                self.layer_params.append(curr_b_nn_layer)
            return hidden_nn_layers[-1], last_layer_size

    def _build_user_embedding(self, hparams):
        T2U_feat_batch = self.iterator.T2U_feat_batch
        T2U_pair_feat_batch = self.iterator.T2U_pair_feat_batch

        T1U_sparse_indexs = tf.SparseTensor(self.iterator.T1U_pair_feat_batch_indices,
                                            self.iterator.T1U_pair_feat_batch_val,
                                            self.iterator.T1U_pair_feat_batch_shape)
        T1U_sparse_weight = tf.SparseTensor(self.iterator.T1U_pair_feat_batch_indices,
                                            self.iterator.T1U_pair_feat_batch_weights,
                                            self.iterator.T1U_pair_feat_batch_shape)
        T1U_embedding = tf.reshape(
            tf.nn.embedding_lookup_sparse(self.T1_pair_feat_embedding, T1U_sparse_indexs, T1U_sparse_weight,
                                          combiner="sum"), [-1, hparams.dim * hparams.T1_pair_field_num])

        T0U_sparse_indexs = tf.SparseTensor(self.iterator.T0U_unique_feat_batch_indices,
                                            self.iterator.T0U_unique_feat_batch_val,
                                            self.iterator.T0U_unique_feat_batch_shape)
        T0U_sparse_weight = tf.SparseTensor(self.iterator.T0U_unique_feat_batch_indices,
                                            self.iterator.T0U_unique_feat_batch_weights,
                                            self.iterator.T0U_unique_feat_batch_shape)
        T0U_embedding = tf.reshape(
            tf.nn.embedding_lookup_sparse(self.T0U_unique_feat_embedding, T0U_sparse_indexs, T0U_sparse_weight,
                                          combiner="sum"), [-1, hparams.dim * hparams.T0U_unique_field_num])

        user_embedding = tf.concat([T2U_feat_batch, T2U_pair_feat_batch, T1U_embedding, T0U_embedding], axis=1)
        user_embed_dim = hparams.T2U_feat_len + hparams.T2_pair_feat_num + hparams.dim * hparams.T1_pair_field_num + hparams.dim * hparams.T0U_unique_field_num
        return user_embedding, user_embed_dim

    def _build_item_embedding(self, hparams):
        T2I_feat_batch = self.iterator.T2I_feat_batch
        T2I_pair_feat_batch = self.iterator.T2I_pair_feat_batch

        T1I_sparse_indexs = tf.SparseTensor(self.iterator.T1I_pair_feat_batch_indices,
                                            self.iterator.T1I_pair_feat_batch_val,
                                            self.iterator.T1I_pair_feat_batch_shape)
        T1I_sparse_weight = tf.SparseTensor(self.iterator.T1I_pair_feat_batch_indices,
                                            self.iterator.T1I_pair_feat_batch_weights,
                                            self.iterator.T1I_pair_feat_batch_shape)
        T1I_embedding = tf.reshape(
            tf.nn.embedding_lookup_sparse(self.T1_pair_feat_embedding, T1I_sparse_indexs, T1I_sparse_weight,
                                          combiner="sum"), [-1, hparams.dim * hparams.T1_pair_field_num])

        T0I_sparse_indexs = tf.SparseTensor(self.iterator.T0I_unique_feat_batch_indices,
                                            self.iterator.T0I_unique_feat_batch_val,
                                            self.iterator.T0I_unique_feat_batch_shape)
        T0I_sparse_weight = tf.SparseTensor(self.iterator.T0I_unique_feat_batch_indices,
                                            self.iterator.T0I_unique_feat_batch_weights,
                                            self.iterator.T0I_unique_feat_batch_shape)
        T0I_embedding = tf.reshape(
            tf.nn.embedding_lookup_sparse(self.T0I_unique_feat_embedding, T0I_sparse_indexs, T0I_sparse_weight,
                                          combiner="sum"), [-1, hparams.dim * hparams.T0I_unique_field_num])

        item_embedding = tf.concat([T2I_feat_batch, T2I_pair_feat_batch, T1I_embedding, T0I_embedding], axis=1)
        item_embed_dim = hparams.T2I_feat_len + hparams.T2_pair_feat_num + hparams.dim * hparams.T1_pair_field_num + hparams.dim * hparams.T0I_unique_field_num
        return item_embedding, item_embed_dim
