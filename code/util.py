"""define util function and  global variable"""
import tensorflow as tf
import time
import sys
import os
import platform
import socket
import logging
import argparse
import random

def check_tensorflow_version():
    if tf.__version__ < "1.2.0":
        raise EnvironmentError("Tensorflow version must >= 1.2.0,but version is {0}". \
                               format(tf.__version__))


def print_time(s, start_time):
    """Take a start time, print elapsed duration, and return a new time."""
    print("%s, %ds, %s." % (s, (time.time() - start_time), time.ctime()))
    sys.stdout.flush()
    return time.time()


def check_file_exist(filename):
    if not os.path.isfile(filename):
        raise ValueError("{0} is not exits".format(filename))


# system
def get_os_name():
    return platform.system()


def get_host_name():
    return platform.node()


def get_host_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    return s.getsockname()[0]


def get_python_version_info():
    return sys.version_info


def get_environment_variable(name, default=None):
    if name not in os.environ:
        return default
    return os.environ[name]

def print_sys_info(logging):
    logging.info("begin")
    logging.info("===============================")
    logging.info("os=%s", get_os_name())
    logging.info("host=%s", get_host_name())
    logging.info("ip=%s", get_host_ip())
    logging.info("python=%s", get_python_version_info()[0:3])

    logging.info("===============================")
    logging.info("tf version=%s", tf.__version__)
    logging.info("CUDA_VISIBLE_DEVICES=%s", get_environment_variable("CUDA_VISIBLE_DEVICES"))
    logging.info("os.environ=%s", os.environ)

    logging.info("===============================")
    logging.info("sys.argv=%s", sys.argv)

    logging.info("begin")
    logging.info("===============================")
    logging.info("os=%s", get_os_name())
    logging.info("host=%s", get_host_name())
    logging.info("ip=%s", get_host_ip())
    logging.info("python=%s", get_python_version_info()[0:3])

    logging.info("===============================")
    logging.info("tf version=%s", tf.__version__)
    logging.info("CUDA_VISIBLE_DEVICES=%s", get_environment_variable("CUDA_VISIBLE_DEVICES"))
    logging.info("os.environ=%s", os.environ)

    logging.info("===============================")
    logging.info("sys.argv=%s", sys.argv)

def split_data(input_file, train_file, test_file):
    train_out = open(train_file, encoding='utf8', mode='w')
    test_out = open(test_file, encoding='utf8', mode='w')
    thread = 0.8
    for line in open(input_file, encoding='utf8'):
        p = random.random()
        if p < thread:
            train_out.write(line)
        else:
            test_out.write(line)
    train_out.close()
    test_out.close()

def sample(input_file, output_file):
    out = open(output_file, encoding='utf8', mode='w')
    for line in open(input_file, encoding='utf8', mode='r'):
        p = random.random()
        if p<0.01:
            out.write(line)
    out.close()

def parser_param_from_cosmos(rootDir, config):
    files = os.listdir(rootDir)
    #parser T0I_unique_field_num
    T0I_unique_field_num_file = 'T0I_field_stat.tsv'
    if T0I_unique_field_num_file in files:
        T0I_unique_field_num = int(open(rootDir+'/'+T0I_unique_field_num_file).readlines()[0])
        if T0I_unique_field_num == 0:
            T0I_unique_field_num = 1
        print(T0I_unique_field_num_file, T0I_unique_field_num)

    # parser T0I_unique_feat_num
    T0I_unique_feat_num_file = 'T0I_feature_stat.tsv'
    if T0I_unique_feat_num_file in files:
        T0I_unique_feat_num = int(open(rootDir + '/' +T0I_unique_feat_num_file).readlines()[0])
        if T0I_unique_feat_num == 0:
            T0I_unique_feat_num = 2
        else:
            T0I_unique_feat_num += 1
        print(T0I_unique_feat_num_file, T0I_unique_feat_num)

    # parser T0U_unique_field_num
    T0U_unique_field_num_file = 'T0U_field_stat.tsv'
    if T0U_unique_field_num_file in files:
        T0U_unique_field_num = int(open(rootDir + '/' +T0U_unique_field_num_file).readlines()[0])
        if T0U_unique_field_num == 0:
            T0U_unique_field_num = 1
        print(T0U_unique_field_num_file, T0U_unique_field_num)

    # parser T0U_unique_feat_num
    T0U_unique_feat_num_file = 'T0U_feature_stat.tsv'
    if T0U_unique_feat_num_file in files:
        T0U_unique_feat_num = int(open(rootDir + '/' +T0U_unique_feat_num_file).readlines()[0])
        if T0U_unique_feat_num == 0:
            T0U_unique_feat_num = 2
        else:
            T0U_unique_feat_num += 1
        print(T0U_unique_feat_num_file, T0U_unique_feat_num)

    # parser T1_pair_field_num
    T1_pair_field_num_file = 'T1_field_stat.tsv'
    if T1_pair_field_num_file in files:
        T1_pair_field_num = int(open(rootDir + '/' +T1_pair_field_num_file).readlines()[0])
        if T1_pair_field_num == 0:
            T1_pair_field_num = 1
        print(T1_pair_field_num_file, T1_pair_field_num)

    # parser T2_pair_feat_num
    T1_pair_feat_num_file = 'T1_feature_stat.tsv'
    if T1_pair_feat_num_file in files:
        T1_pair_feat_num = int(open(rootDir + '/' +T1_pair_feat_num_file).readlines()[0])
        if T1_pair_feat_num == 0:
            T1_pair_feat_num = 2
        else:
            T1_pair_feat_num += 1
        print(T1_pair_feat_num_file, T1_pair_feat_num)

    # parser T2U_feat_len
    T2U_feat_len_file = 'T2U_feat_stat.txt'
    if T2U_feat_len_file in files:
        T2U_feat_len = int(open(rootDir + '/' +T2U_feat_len_file).readlines()[0])
    else:
        T2U_feat_len = 1
    print(T2U_feat_len_file, T2U_feat_len)

    # parser T2I_feat_len
    T2I_feat_len_file = 'T2I_feat_stat.txt'
    if T2I_feat_len_file in files:
        T2I_feat_len = int(open(rootDir + '/' +T2I_feat_len_file).readlines()[0])
    else:
        T2I_feat_len = 1
    print(T2I_feat_len_file, T2I_feat_len)

    #T2_pair_feat_num
    T2_pair_feat_file = 'T2_pair_feat_stat.txt'
    if T2_pair_feat_file in files:
        T2_pair_feat_num = int(open(rootDir + '/' +T2_pair_feat_file).readlines()[0])
    else:
        T2_pair_feat_num = 1
    print(T2_pair_feat_file, T2_pair_feat_num)
    config['T2U_feat_len'] = T2U_feat_len
    config['T2I_feat_len'] = T2I_feat_len
    config['T2_pair_feat_num'] = T2_pair_feat_num
    config['T1_pair_field_num'] = T1_pair_field_num
    config['T1_pair_feat_num'] = T1_pair_feat_num

    config['T0U_unique_field_num'] = T0U_unique_field_num
    config['T0U_unique_feat_num'] = T0U_unique_feat_num
    config['T0I_unique_field_num'] = T0I_unique_field_num
    config['T0I_unique_feat_num'] = T0I_unique_feat_num