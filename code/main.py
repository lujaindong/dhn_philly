"""This script parse and run train function"""
import train
from util import *
import tensorflow as tf
import sys
import os
import platform
import socket
import logging
import argparse
import random
from infer import infer_embedding
import subprocess

# import shutil


logging.basicConfig(stream=sys.stdout, format='[%(asctime)s] %(levelname)s: %(message)s', level=logging.INFO)
print_sys_info(logging)


def GetParsedArguments(pattern):
    parsed_results = {}
    parser = argparse.ArgumentParser()
    for arg in sys.argv:
        if arg.startswith(pattern):
            parser.add_argument(arg, nargs='?')

    (args, unknown) = parser.parse_known_args()

    for arg in sorted(vars(args)):
        value = getattr(args, arg)
        parsed_results[arg] = value
    return parsed_results


dic_input_dir = GetParsedArguments("--input-")
dic_output_dir = GetParsedArguments("--output-")
layer_sizes_dir = GetParsedArguments("--layer_sizes")
activation_dir = GetParsedArguments("--activation")
dim_dir = GetParsedArguments("--dim")
init_value_dir = GetParsedArguments("--init_value")
learning_rate_dir = GetParsedArguments("--learning_rate")
epochs_dir = GetParsedArguments("--epochs")
batch_size_dir = GetParsedArguments("--batch_size")
eval_flag_dir = GetParsedArguments("--eval_flag")
user_embed_flag_dir = GetParsedArguments("--user_embed_flag")
show_step_dir = GetParsedArguments("--show_step")
save_epoch_dir = GetParsedArguments("--save_epoch")
regular_dir = GetParsedArguments("--regular")

if dic_input_dir is None or dic_output_dir is None:
    logging.info("can not parse input/ouput arguments")
    sys.exit(0)

logging.info(dic_input_dir)
logging.info(dic_output_dir)


def create_hparams(FLAGS):
    """Create hparams."""
    return tf.contrib.training.HParams(
        # data
        train_file=FLAGS['train_file'] if 'train_file' in FLAGS else None,
        eval_file=FLAGS['eval_file'] if 'eval_file' in FLAGS else None,
        infer_file=FLAGS['infer_file'] if 'infer_file' in FLAGS else None,
        T2U_feat_len=FLAGS['T2U_feat_len'] if 'T2U_feat_len' in FLAGS else None,
        T2I_feat_len=FLAGS['T2I_feat_len'] if 'T2I_feat_len' in FLAGS else None,
        T2_pair_feat_num=FLAGS['T2_pair_feat_num'] if 'T2_pair_feat_num' in FLAGS else None,
        T1_pair_field_num=FLAGS['T1_pair_field_num'] if 'T1_pair_field_num' in FLAGS else None,
        T1_pair_feat_num=FLAGS['T1_pair_feat_num'] if 'T1_pair_feat_num' in FLAGS else None,
        T0U_unique_field_num=FLAGS['T0U_unique_field_num'] if 'T0U_unique_field_num' in FLAGS else None,
        T0U_unique_feat_num=FLAGS['T0U_unique_feat_num'] if 'T0U_unique_feat_num' in FLAGS else None,
        T0I_unique_field_num=FLAGS['T0I_unique_field_num'] if 'T0I_unique_field_num' in FLAGS else None,
        T0I_unique_feat_num=FLAGS['T0I_unique_feat_num'] if 'T0I_unique_feat_num' in FLAGS else None,

        # model
        model_type=FLAGS['model_type'] if 'model_type' in FLAGS else None,
        layer_sizes=FLAGS['layer_sizes'] if 'layer_sizes' in FLAGS else None,
        activation=FLAGS['activation'] if 'activation' in FLAGS else None,
        load_model_name=FLAGS['load_model_name'] if 'load_model_name' in FLAGS else None,
        dim=FLAGS['dim'] if 'dim' in FLAGS else None,

        # train
        init_method=FLAGS['init_method'] if 'init_method' in FLAGS else 'tnormal',
        init_value=FLAGS['init_value'] if 'init_value' in FLAGS else 0.01,
        embed_l2=FLAGS['embed_l2'] if 'embed_l2' in FLAGS else 0.0000,
        embed_l1=FLAGS['embed_l1'] if 'embed_l1' in FLAGS else 0.0000,
        layer_l2=FLAGS['layer_l2'] if 'layer_l2' in FLAGS else 0.0000,
        layer_l1=FLAGS['layer_l1'] if 'layer_l1' in FLAGS else 0.0000,
        learning_rate=FLAGS['learning_rate'] if 'learning_rate' in FLAGS else 0.001,
        loss=FLAGS['loss'] if 'loss' in FLAGS else None,
        optimizer=FLAGS['optimizer'] if 'optimizer' in FLAGS else 'adam',
        epochs=FLAGS['epochs'] if 'epochs' in FLAGS else 10,
        batch_size=FLAGS['batch_size'] if 'batch_size' in FLAGS else 1,

        # show info
        show_step=FLAGS['show_step'] if 'show_step' in FLAGS else 1,
        metrics=FLAGS['metrics'] if 'metrics' in FLAGS else None,
        save_epoch=FLAGS['save_epoch'] if 'save_epoch' in FLAGS else None,

        # save model
        model_dir=FLAGS['model_dir'] if 'model_dir' in FLAGS else None,
        cache_path=FLAGS['cache_path'] if 'cache_path' in FLAGS else None,
        res_dir=FLAGS['res_dir'] if 'res_dir' in FLAGS else None,

        # other flag
        eval_flag=FLAGS['eval_flag'] if 'eval_flag' in FLAGS else None,
        user_embed_flag=FLAGS['user_embed_flag'] if 'user_embed_flag' in FLAGS else None
    )


def parser_cmd(config):
    config['layer_sizes'] = [int(val) for val in layer_sizes_dir['layer_sizes'].strip().split('#')]
    config['activation'] = activation_dir['activation'].strip().split('#')
    config['dim'] = int(dim_dir['dim'])
    config['init_value'] = float(init_value_dir['init_value'])
    config['learning_rate'] = float(learning_rate_dir['learning_rate'])
    config['epochs'] = int(epochs_dir['epochs'])
    config['batch_size'] = int(batch_size_dir['batch_size'])
    config['eval_flag'] = int(eval_flag_dir['eval_flag'])
    config['show_step'] = int(show_step_dir['show_step'])
    config['save_epoch'] = int(save_epoch_dir['save_epoch'])
    config['user_embed_flag'] = int(user_embed_flag_dir['user_embed_flag'])
    config['embed_l2'] = float(regular_dir['regular'])
    config['embed_l1'] = float(regular_dir['regular'])
    config['layer_l2'] = float(regular_dir['regular'])
    config['layer_l1'] = float(regular_dir['regular'])


def main():
    """main function"""
    # flag = True
    config = {
        'model_type': 'Dhn',
        'init_method': 'tnormal',
        'loss': 'square_loss',
        'optimizer': 'adam',
    }

    # parser parameter form command
    parser_cmd(config)
    # source_data_file = dic_input_dir['input_training_data_path'] + '\\ad_toy_data.txt'
    # data_file = dic_input_dir['input_training_data_path'] + '\\ad_toy_data_sample.txt'
    # sample data
    # sample(source_data_file, data_file)

    # must be set according the file name
    source_train_file = dic_input_dir['input_training_data_path'] + 'final_train_file.tsv'
    train_file = dic_input_dir['input_training_data_path'] + 'final_train_file_sample.tsv'
    # sample(source_train_file, train_file)
    train_file = train_file

    source_eval_file = dic_input_dir['input_training_data_path'] + 'final_test_file.tsv'
    eval_file = dic_input_dir['input_training_data_path'] + 'final_test_file_sample.tsv'
    # sample(source_eval_file, eval_file)
    eval_file = eval_file

    source_infer_path = dic_input_dir['input_training_data_path'] + 'user_embedding_input.tsv'
    infer_path = dic_input_dir['input_training_data_path'] + 'user_embedding_input_sample.tsv'
    # sample(source_infer_path, infer_path)
    infer_path = infer_path

    parser_param_from_cosmos(dic_input_dir['input_training_data_path'], config)
    # data_file = dic_input_dir['input_training_data_path'] + '\\ad_toy_data.txt'
    config['train_file'] = train_file
    config['eval_file'] = eval_file
    config['infer_file'] = infer_path
    cache_dir = dic_input_dir['input_training_data_path'] + 'cache_v1/'
    dic_input_dir['cache_path'] = cache_dir
    cmd = 'rm -r ' + cache_dir
    subprocess.call(cmd, shell=True)
    if not os.path.isdir(dic_input_dir['cache_path']):
        os.makedirs(dic_input_dir['cache_path'], exist_ok=True)
    config['cache_path'] = cache_dir

    initial_model_path = dic_output_dir['output_model_path']
    model_dir = initial_model_path + '/model_v1/'
    dic_output_dir['output_model_path'] = model_dir
    cmd = 'rm -r ' + model_dir
    subprocess.call(cmd, shell=True)
    dic_output_dir['output_model_path'] = model_dir
    if not os.path.isdir(dic_output_dir['output_model_path']):
        os.makedirs(dic_output_dir['output_model_path'], exist_ok=True)
    config['model_dir'] = model_dir
    # remove old model file
    # shutil.rmtree(config['model_dir'])

    res_dir = initial_model_path + '/res_v1/'
    dic_output_dir['output_res_path'] = res_dir
    if not os.path.isdir(dic_output_dir['output_res_path']):
        os.makedirs(dic_output_dir['output_res_path'], exist_ok=True)
    config['res_dir'] = res_dir

    hparams = create_hparams(config)
    train.train(hparams)
    logging.info("training end")


main()
