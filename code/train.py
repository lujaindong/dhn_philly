"""define train, infer, eval, test process"""
import numpy as np
import os, time, collections
import tensorflow as tf
from iterator import DhnIterator
from dhn import DhnModel
from dhn_cache import DhnCache
import util as util
import logging
import sys
from math import log
from datetime import datetime, timedelta, timezone
from sklearn.metrics import roc_auc_score, log_loss, mean_squared_error
import infer as infer


class TrainModel(collections.namedtuple("TrainModel", ("graph", "model", "iterator", "filenames"))):
    """define train class, include graph, model, iterator"""
    pass


def create_train_model(model_creator, hparams, scope=None):
    graph = tf.Graph()
    with graph.as_default():
        filenames = tf.placeholder(tf.string, shape=[None])
        src_dataset = tf.contrib.data.TFRecordDataset(filenames)
        # 构建训练数据的迭代器
        batch_input = DhnIterator(src_dataset, hparams)
        # 创建model
        model = model_creator(
            hparams,
            mode=tf.contrib.learn.ModeKeys.TRAIN,
            iterator=batch_input,
            scope=scope)
    # 返回train graph, 含有graph, model, iterator
    return TrainModel(
        graph=graph,
        model=model,
        iterator=batch_input,
        filenames=filenames)


# run evaluation and get evaluted loss
def run_eval(load_model, load_sess, filename, hparams, flag):
    print(filename)
    load_sess.run(load_model.iterator.initializer, feed_dict={load_model.filenames: [filename]})
    preds = []
    labels = []
    while True:
        try:
            _, _, step_pred, step_labels = load_model.model.eval(load_sess)
            preds.extend(np.reshape(step_pred, -1))
            labels.extend(np.reshape(step_labels, -1))
        except tf.errors.OutOfRangeError:
            break
    logloss = log_loss(np.asarray(labels), np.asarray(preds))
    res = {}
    # res['logloss'] = round(logloss, 4)
    res['mean_squared_error'] = mean_squared_error(np.asarray(labels), np.asarray(preds))
    res['auc'] = roc_auc_score(np.asarray(labels), np.asarray(preds))
    return res


# cache data
def cache_data(hparams, filename, flag):
    cache_obj = DhnCache()
    if flag == 'train':
        hparams.train_file_cache = hparams.cache_path + 'train_batch_size_' + str(hparams.batch_size)
        cached_name = hparams.train_file_cache
    elif flag == 'eval':
        hparams.eval_file_cache = hparams.cache_path + 'eval_batch_size_' + str(hparams.batch_size)
        cached_name = hparams.eval_file_cache
    else:
        raise ValueError("flag must be train, eval")
    print('cache filename:', filename)
    if not os.path.isfile(cached_name):
        print('has not cached file, begin cached...')
        start_time = time.time()
        cache_obj.write_tfrecord(filename, cached_name, hparams)
        util.print_time("caced file used time", start_time)


def train(hparams, scope=None, target_session=""):
    logging.basicConfig(stream=sys.stdout, format='[%(asctime)s] %(levelname)s: %(message)s', level=logging.INFO)
    logging.info("begin")
    logging.info("===============================")
    params = hparams.values()
    for key, val in params.items():
        logging.info(str(key) + ':' + str(val))

    print('load and cache data...')
    if hparams.train_file is not None:
        cache_data(hparams, hparams.train_file, flag='train')

    if hparams.eval_flag:
        if hparams.eval_file is not None:
            cache_data(hparams, hparams.eval_file, flag='eval')

    if hparams.model_type == 'Dhn':
        model_creator = DhnModel
        batch_input = DhnIterator
        cache_obj = DhnCache
    else:
        raise ValueError("model type is not defined!")
    log_file = hparams.res_dir + '/res.txt'
    log_writer = open(log_file, 'w')
    train_model = create_train_model(model_creator, hparams, scope)
    train_sess = tf.Session(target=target_session, graph=train_model.graph)
    with train_sess.as_default(), train_model.graph.as_default():
        train_sess.run(train_model.model.init_op)
        # load model from checkpoint
        # if not hparams.load_model_name is None:
        #     checkpoint_path = hparams.load_model_name
        #     try:
        #         train_model.model.saver.restore(train_sess, checkpoint_path)
        #         logging.info('load model', checkpoint_path)
        #     except:
        #         raise IOError("Failed to find any matching files for {0}".format(checkpoint_path))

        step = 0
        for epoch in range(hparams.epochs):
            train_sess.run(train_model.iterator.initializer,
                           feed_dict={train_model.filenames: [hparams.train_file_cache]})
            epoch_loss = 0
            train_start = time.time()
            train_load_time = 0
            while True:
                try:
                    t1 = time.time()
                    step_result = train_model.model.train(train_sess)
                    t3 = time.time()
                    train_load_time += t3 - t1
                    (_, step_loss, step_data_loss, summary) = step_result
                    epoch_loss += step_loss
                    step += 1
                    if step % hparams.show_step == 0:
                        logging.info('step {0:d} , loss: {1:.4f}, data loss: {2:.4f}' \
                                     .format(step, step_loss, step_data_loss))
                except tf.errors.OutOfRangeError:
                    logging.info('finish one epoch!')
                    break
            train_end = time.time()
            train_time = train_end - train_start
            if epoch % hparams.save_epoch == 0:
                # checkpoint_path = train_model.model.saver.save(sess=train_sess,
                #         save_path=util.MODEL_DIR + 'epoch_' + str(epoch))
                utc_dt = datetime.utcnow().replace(tzinfo=timezone.utc)
                bj_dt = utc_dt.astimezone(timezone(timedelta(hours=8)))
                # save_model_dir = hparams.model_dir + bj_dt.strftime('%Y-%m-%d_%H_%M_%S') + '_epoch_' + str(epoch)
                save_model_dir = hparams.model_dir + 'epoch_' + str(epoch)
                builder = tf.saved_model.builder.SavedModelBuilder(save_model_dir)
                builder.add_meta_graph_and_variables(train_sess, ['tag_string'])
                builder.save()

            eval_start = time.time()
            train_res = run_eval(train_model, train_sess, hparams.train_file_cache, hparams, flag='train')
            train_info = ', '.join(
                [str(item[0]) + ':' + str(item[1])
                 for item in sorted(train_res.items(), key=lambda x: x[0])])

            if hparams.eval_flag:
                eval_res = run_eval(train_model, train_sess, hparams.eval_file_cache, hparams, flag='eval')
                eval_info = ', '.join(
                    [str(item[0]) + ':' + str(item[1])
                     for item in sorted(eval_res.items(), key=lambda x: x[0])])
            else:
                eval_info = 'NULL'

            eval_end = time.time()
            eval_time = eval_end - eval_start

            logging.info('at epoch {0:d}'.format(epoch) + ' train info: ' + train_info + ' eval info: ' + eval_info)
            logging.info('at epoch {0:d} , train time: {1:.1f} eval time: {2:.1f}'.format(epoch, train_time, eval_time))
            logging.info('\n')
            line = 'at epoch {0:d}'.format(epoch) + ' train info: ' + train_info + ' eval info: ' + eval_info
            log_writer.write(line + '\n')
            line = 'at epoch {0:d} , train time: {1:.1f} eval time: {2:.1f}'.format(epoch, train_time, eval_time)
            log_writer.write(line + '\n')
        if hparams.user_embed_flag:
            # finished training , infer the user embedding
            logging.info("begin infer!")
            # using final epoch model for infer
            # infer_path = dic_input_dir['input_training_data_path'] + '\\user_embedding_input.txt'
            infer_cached_path = hparams.cache_path + 'infer_embedding.tfrecord'
            res_path = hparams.res_dir + 'emebdding_res.txt'
            infer.infer_embedding_from_session(hparams.infer_file, infer_cached_path, train_sess, res_path, hparams)
            logging.info("end infer!")
