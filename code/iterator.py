"""define iterator"""
import collections
import tensorflow as tf
import abc

BUFFER_SIZE = 256
__all__ = ["BaseIterator", "DhnIterator"]


class BaseIterator(object):
    @abc.abstractmethod
    def get_iterator(self, src_dataset):
        """Subclass must implement this."""
        pass

    @abc.abstractmethod
    def parser(self, record):
        pass


class DhnIterator(BaseIterator):
    def __init__(self, src_dataset, hparams):
        self.hparams = hparams
        self.get_iterator(src_dataset)

    def get_iterator(self, src_dataset):
        src_dataset = src_dataset.map(self.parser)
        # shuffle
        # src_dataset = src_dataset.shuffle(buffer_size=BUFFER_SIZE)
        iterator = src_dataset.make_initializable_iterator()
        labels, \
        T2I_pair_feat_batch, T2U_pair_feat_batch, \
        T2I_feat_batch, T2U_feat_batch, \
        T1I_pair_feat_batch_indices, T1I_pair_feat_batch_val, \
        T1I_pair_feat_batch_weights, T1I_pair_feat_batch_shape, \
        T1U_pair_feat_batch_indices, T1U_pair_feat_batch_val, \
        T1U_pair_feat_batch_weights, T1U_pair_feat_batch_shape, \
        T0I_unique_feat_batch_indices, T0I_unique_feat_batch_val, \
        T0I_unique_feat_batch_weights, T0I_unique_feat_batch_shape, \
        T0U_unique_feat_batch_indices, T0U_unique_feat_batch_val, \
        T0U_unique_feat_batch_weights, T0U_unique_feat_batch_shape = iterator.get_next()
        self.initializer = iterator.initializer

        self.T2I_pair_feat_batch = tf.identity(T2I_pair_feat_batch, name='T2I_pair_feat_batch')
        self.T2U_pair_feat_batch = tf.identity(T2U_pair_feat_batch, name='T2U_pair_feat_batch')
        self.T2I_feat_batch = tf.identity(T2I_feat_batch, name='T2I_feat_batch')
        self.T2U_feat_batch = tf.identity(T2U_feat_batch, name='T2U_feat_batch')
        self.labels = tf.identity(labels, name='labels')
        # print('T2I_pair_feat_batch:', self.T2I_pair_feat_batch)
        # print('T2U_pair_feat_batch:', self.T2U_pair_feat_batch)
        # print('T2I_feat_batch:', self.T2I_feat_batch)
        # print('T2U_feat_batch:', self.T2U_feat_batch)


        self.T1I_pair_feat_batch_indices = tf.identity(T1I_pair_feat_batch_indices, name='T1I_pair_feat_batch_indices')
        self.T1I_pair_feat_batch_val = tf.identity(T1I_pair_feat_batch_val, name='T1I_pair_feat_batch_val')
        self.T1I_pair_feat_batch_weights = tf.identity(T1I_pair_feat_batch_weights, name='T1I_pair_feat_batch_weights')
        self.T1I_pair_feat_batch_shape = tf.identity(T1I_pair_feat_batch_shape, name='T1I_pair_feat_batch_shape')
        # print('T1I_pair_feat_batch_indices:', self.T1I_pair_feat_batch_indices)
        # print('T1I_pair_feat_batch_val:', self.T1I_pair_feat_batch_val)
        # print('T1I_pair_feat_batch_weights:', self.T1I_pair_feat_batch_weights)
        # print('T1I_pair_feat_batch_shape:', self.T1I_pair_feat_batch_shape)

        self.T1U_pair_feat_batch_indices = tf.identity(T1U_pair_feat_batch_indices, name='T1U_pair_feat_batch_indices')
        self.T1U_pair_feat_batch_val = tf.identity(T1U_pair_feat_batch_val, name='T1U_pair_feat_batch_val')
        self.T1U_pair_feat_batch_weights = tf.identity(T1U_pair_feat_batch_weights, name='T1U_pair_feat_batch_weights')
        self.T1U_pair_feat_batch_shape = tf.identity(T1U_pair_feat_batch_shape, name='T1U_pair_feat_batch_shape')
        # print('T1U_pair_feat_batch_indices:', self.T1U_pair_feat_batch_indices)
        # print('T1U_pair_feat_batch_val:', self.T1U_pair_feat_batch_val)
        # print('T1U_pair_feat_batch_weights:', self.T1U_pair_feat_batch_weights)
        # print('T1U_pair_feat_batch_shape:', self.T1U_pair_feat_batch_shape)

        self.T0I_unique_feat_batch_indices = tf.identity(T0I_unique_feat_batch_indices, name='T0I_unique_feat_batch_indices')
        self.T0I_unique_feat_batch_val = tf.identity(T0I_unique_feat_batch_val, name='T0I_unique_feat_batch_val')
        self.T0I_unique_feat_batch_weights = tf.identity(T0I_unique_feat_batch_weights, name='T0I_unique_feat_batch_weights')
        self.T0I_unique_feat_batch_shape = tf.identity(T0I_unique_feat_batch_shape, name='T0I_unique_feat_batch_shape')
        # print('T0I_unique_feat_batch_indices:', self.T0I_unique_feat_batch_indices)
        # print('T0I_unique_feat_batch_val:', self.T0I_unique_feat_batch_val)
        # print('T0I_unique_feat_batch_weights:', self.T0I_unique_feat_batch_weights)
        # print('T0I_unique_feat_batch_shape:', self.T0I_unique_feat_batch_shape)


        self.T0U_unique_feat_batch_indices = tf.identity(T0U_unique_feat_batch_indices, name='T0U_unique_feat_batch_indices')
        self.T0U_unique_feat_batch_val = tf.identity(T0U_unique_feat_batch_val, name='T0U_unique_feat_batch_val')
        self.T0U_unique_feat_batch_weights = tf.identity(T0U_unique_feat_batch_weights, name='T0U_unique_feat_batch_weights')
        self.T0U_unique_feat_batch_shape = tf.identity(T0U_unique_feat_batch_shape, name='T0U_unique_feat_batch_shape')
        # print('T0U_unique_feat_batch_indices:', self.T0U_unique_feat_batch_indices)
        # print('T0U_unique_feat_batch_val:', self.T0U_unique_feat_batch_val)
        # print('T0U_unique_feat_batch_weights:', self.T0U_unique_feat_batch_weights)
        # print('T0U_unique_feat_batch_shape:', self.T0U_unique_feat_batch_shape)

    def parser(self, record):
        keys_to_features = {
            'T2I_pair_feat_batch': tf.FixedLenFeature([], tf.string),
            'T2U_pair_feat_batch': tf.FixedLenFeature([], tf.string),
            'T2I_feat_batch': tf.FixedLenFeature([], tf.string),
            'T2U_feat_batch': tf.FixedLenFeature([], tf.string),
            'labels': tf.FixedLenFeature([], tf.string),
            'T1I_pair_feat_batch_indices': tf.FixedLenFeature([], tf.string),
            'T1I_pair_feat_batch_val': tf.VarLenFeature(tf.int64),
            'T1I_pair_feat_batch_weights': tf.VarLenFeature(tf.float32),
            'T1I_pair_feat_batch_shape': tf.FixedLenFeature([2], tf.int64),
            'T1U_pair_feat_batch_indices': tf.FixedLenFeature([], tf.string),
            'T1U_pair_feat_batch_val': tf.VarLenFeature(tf.int64),
            'T1U_pair_feat_batch_weights': tf.VarLenFeature(tf.float32),
            'T1U_pair_feat_batch_shape': tf.FixedLenFeature([2], tf.int64),
            'T0U_unique_feat_batch_indices': tf.FixedLenFeature([], tf.string),
            'T0U_unique_feat_batch_val': tf.VarLenFeature(tf.int64),
            'T0U_unique_feat_batch_weights': tf.VarLenFeature(tf.float32),
            'T0U_unique_feat_batch_shape': tf.FixedLenFeature([2], tf.int64),
            'T0I_unique_feat_batch_indices': tf.FixedLenFeature([], tf.string),
            'T0I_unique_feat_batch_val': tf.VarLenFeature(tf.int64),
            'T0I_unique_feat_batch_weights': tf.VarLenFeature(tf.float32),
            'T0I_unique_feat_batch_shape': tf.FixedLenFeature([2], tf.int64)
        }

        parsed = tf.parse_single_example(record, keys_to_features)
        T2I_pair_feat_batch = tf.reshape(tf.decode_raw(parsed['T2I_pair_feat_batch'], tf.float32),
                                         [-1, self.hparams.T2_pair_feat_num])
        T2U_pair_feat_batch = tf.reshape(tf.decode_raw(parsed['T2U_pair_feat_batch'], tf.float32),
                                         [-1, self.hparams.T2_pair_feat_num])
        T2I_feat_batch = tf.reshape(tf.decode_raw(parsed['T2I_feat_batch'], tf.float32),
                                    [-1, self.hparams.T2I_feat_len])
        T2U_feat_batch = tf.reshape(tf.decode_raw(parsed['T2U_feat_batch'], tf.float32),
                                    [-1, self.hparams.T2U_feat_len])
        labels = tf.reshape(tf.decode_raw(parsed['labels'], tf.float32), [-1, 1])

        T1I_pair_feat_batch_indices = tf.reshape(tf.decode_raw(parsed['T1I_pair_feat_batch_indices'], tf.int64),
                                                 [-1, 2])
        T1I_pair_feat_batch_val = tf.sparse_tensor_to_dense(parsed['T1I_pair_feat_batch_val'])
        T1I_pair_feat_batch_weights = tf.sparse_tensor_to_dense(parsed['T1I_pair_feat_batch_weights'])
        T1I_pair_feat_batch_shape = parsed['T1I_pair_feat_batch_shape']

        T1U_pair_feat_batch_indices = tf.reshape(tf.decode_raw(parsed['T1U_pair_feat_batch_indices'], tf.int64),
                                                 [-1, 2])
        T1U_pair_feat_batch_val = tf.sparse_tensor_to_dense(parsed['T1U_pair_feat_batch_val'])
        T1U_pair_feat_batch_weights = tf.sparse_tensor_to_dense(parsed['T1U_pair_feat_batch_weights'])
        T1U_pair_feat_batch_shape = parsed['T1U_pair_feat_batch_shape']

        T0I_unique_feat_batch_indices = tf.reshape(tf.decode_raw(parsed['T0I_unique_feat_batch_indices'], tf.int64),
                                                   [-1, 2])
        T0I_unique_feat_batch_val = tf.sparse_tensor_to_dense(parsed['T0I_unique_feat_batch_val'])
        T0I_unique_feat_batch_weights = tf.sparse_tensor_to_dense(parsed['T0I_unique_feat_batch_weights'])
        T0I_unique_feat_batch_shape = parsed['T0I_unique_feat_batch_shape']

        T0U_unique_feat_batch_indices = tf.reshape(tf.decode_raw(parsed['T0U_unique_feat_batch_indices'], tf.int64),
                                                   [-1, 2])
        T0U_unique_feat_batch_val = tf.sparse_tensor_to_dense(parsed['T0U_unique_feat_batch_val'])
        T0U_unique_feat_batch_weights = tf.sparse_tensor_to_dense(parsed['T0U_unique_feat_batch_weights'])
        T0U_unique_feat_batch_shape = parsed['T0U_unique_feat_batch_shape']

        return labels, \
               T2I_pair_feat_batch, T2U_pair_feat_batch, \
               T2I_feat_batch, T2U_feat_batch, \
               T1I_pair_feat_batch_indices, T1I_pair_feat_batch_val, \
               T1I_pair_feat_batch_weights, T1I_pair_feat_batch_shape, \
               T1U_pair_feat_batch_indices, T1U_pair_feat_batch_val, \
               T1U_pair_feat_batch_weights, T1U_pair_feat_batch_shape, \
               T0I_unique_feat_batch_indices, T0I_unique_feat_batch_val, \
               T0I_unique_feat_batch_weights, T0I_unique_feat_batch_shape, \
               T0U_unique_feat_batch_indices, T0U_unique_feat_batch_val, \
               T0U_unique_feat_batch_weights, T0U_unique_feat_batch_shape

        # 测试代码


def test_DhnIterator():
    def create_hparams():
        """Create hparams."""
        return tf.contrib.training.HParams(
            batch_size=1024,
            T2U_feat_len=24,
            T2I_feat_len=6,
            T2_pair_feat_num=128,
            T1_pair_field_num=5,
            T1_pair_feat_num=50592,
            T0U_unique_field_num=13,
            T0U_unique_feat_num=115834,
            T0I_unique_field_num=1,
            T0I_unique_feat_num=2
        )

    hparams = create_hparams()
    outfile = '../data/cache/train_batch_size_1024'
    filenames = [outfile]
    src_dataset = tf.contrib.data.TFRecordDataset(filenames)
    batch_iter = DhnIterator(src_dataset, hparams)
    sess = tf.Session()
    for _ in range(1):
        sess.run(batch_iter.initializer)
        while True:
            try:
                # print(sess.run([batch_iter.labels]))
                # print(sess.run([batch_iter.T2I_pair_feat_batch, batch_iter.T2U_pair_feat_batch]))
                # print(sess.run([batch_iter.T2I_feat_batch, batch_iter.T2U_feat_batch]))
                # print(sess.run([batch_iter.T1I_pair_feat_batch_indices, batch_iter.T1I_pair_feat_batch_val, \
                #                batch_iter.T1I_pair_feat_batch_weights, batch_iter.T1I_pair_feat_batch_shape]))
                # print(sess.run([batch_iter.T1U_pair_feat_batch_indices, batch_iter.T1U_pair_feat_batch_val, \
                #               batch_iter.T1U_pair_feat_batch_weights, batch_iter.T1U_pair_feat_batch_shape]))
                #print(sess.run([batch_iter.T0I_unique_feat_batch_indices, batch_iter.T0I_unique_feat_batch_val, \
                #                batch_iter.T0I_unique_feat_batch_weights, batch_iter.T0I_unique_feat_batch_shape]))
                print(sess.run([batch_iter.T0I_unique_feat_batch_shape]))
                # print(sess.run([batch_iter.T0U_unique_feat_batch_indices, batch_iter.T0U_unique_feat_batch_val,\
                #                batch_iter.T0U_unique_feat_batch_weights, batch_iter.T0U_unique_feat_batch_shape]))
            except tf.errors.OutOfRangeError:
                print("end")
                break

#test_DhnIterator()
