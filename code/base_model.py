"""define base class model"""
import abc
import math
import tensorflow as tf
import util as util
from iterator import BaseIterator

__all__ = ["BaseModel"]


class BaseModel(object):
    def __init__(self, hparams, mode, iterator, scope=None):
        assert isinstance(iterator, BaseIterator)
        self.iterator = iterator
        self.mode = mode
        self.layer_params = []
        with tf.device('/cpu:0'):
            with tf.variable_scope("T1_pair_feat_embedding"):
                self.T1_pair_feat_embedding = tf.get_variable(name='T1_pair_feat_embedding_layer', \
                                                              shape=[hparams.T1_pair_feat_num, hparams.dim], \
                                                              dtype=tf.float32, \
                                                              initializer=tf.truncated_normal_initializer( \
                                                                  mean=0, stddev=hparams.init_value))
        with tf.device('/cpu:0'):
            with tf.variable_scope("T0U_unique_feat_embedding"):
                self.T0U_unique_feat_embedding = tf.get_variable(name='T0U_unique_feat_embedding_layer', \
                                                                 shape=[hparams.T0U_unique_feat_num, hparams.dim], \
                                                                 dtype=tf.float32, \
                                                                 initializer=tf.truncated_normal_initializer( \
                                                                     mean=0, stddev=hparams.init_value))
        with tf.device('/cpu:0'):
            with tf.variable_scope("T0I_unique_feat_embedding"):
                self.T0I_unique_feat_embedding = tf.get_variable(name='T0I_unique_feat_embedding_layer', \
                                                                 shape=[hparams.T0I_unique_feat_num, hparams.dim], \
                                                                 dtype=tf.float32, \
                                                                 initializer=tf.truncated_normal_initializer( \
                                                                     mean=0, stddev=hparams.init_value))

        self.initializer = self._get_initializer(hparams)
        self.logit = self._build_graph(hparams)
        if hparams.loss == 'log_loss':
            self.pred = tf.sigmoid(self.logit)
        else:
            self.pred = self.logit

        self.data_loss = self._compute_data_loss(hparams)
        self.regular_loss = self._compute_regular_loss(hparams)
        self.loss = tf.add(self.data_loss, self.regular_loss)
        self.saver = tf.train.Saver(max_to_keep=hparams.epochs)
        if self.mode == tf.contrib.learn.ModeKeys.TRAIN:
            self.update = self._build_train_opt(hparams)
            self.init_op = tf.global_variables_initializer()
        #
        self.merged = self._add_summaries()

    def _add_summaries(self):
        tf.summary.scalar("data_loss", self.data_loss)
        tf.summary.scalar("regular_loss", self.regular_loss)
        tf.summary.scalar("loss", self.loss)
        tf.summary.histogram("predict_prob", self.pred)
        merged = tf.summary.merge_all()
        return merged

    @abc.abstractmethod
    def _build_graph(self, hparams):
        """Subclass must implement this."""
        pass

    def _l2_loss(self, hparams):
        l2_loss = tf.zeros([1], dtype=tf.float32)
        l2_loss = tf.add(l2_loss, tf.multiply(hparams.embed_l2, tf.nn.l2_loss(self.T1_pair_feat_embedding)))
        l2_loss = tf.add(l2_loss, tf.multiply(hparams.embed_l2, tf.nn.l2_loss(self.T0U_unique_feat_embedding)))
        l2_loss = tf.add(l2_loss, tf.multiply(hparams.embed_l2, tf.nn.l2_loss(self.T0I_unique_feat_embedding)))
        params = self.layer_params
        for param in params:
            l2_loss = tf.add(l2_loss, tf.multiply(hparams.layer_l2, tf.nn.l2_loss(param)))
        return l2_loss

    def _l1_loss(self, hparams):
        l1_loss = tf.zeros([1], dtype=tf.float32)
        l1_loss = tf.add(l1_loss, tf.multiply(hparams.embed_l1, tf.norm(self.T1_pair_feat_embedding, ord=1)))
        l1_loss = tf.add(l1_loss, tf.multiply(hparams.embed_l1, tf.norm(self.T0U_unique_feat_embedding, ord=1)))
        l1_loss = tf.add(l1_loss, tf.multiply(hparams.embed_l1, tf.norm(self.T0I_unique_feat_embedding, ord=1)))
        params = self.layer_params
        for param in params:
            l1_loss = tf.add(l1_loss, tf.multiply(hparams.layer_l1, tf.norm(param, ord=1)))
        return l1_loss

    def _get_initializer(self, hparams):
        if hparams.init_method == 'tnormal':
            return tf.truncated_normal_initializer(stddev=hparams.init_value)
        elif hparams.init_method == 'uniform':
            return tf.random_uniform_initializer(-hparams.init_value, hparams.init_value)
        elif hparams.init_method == 'normal':
            return tf.random_normal_initializer(stddev=hparams.init_value)
        elif hparams.init_method == 'xavier_normal':
            return tf.contrib.layers.xavier_initializer(uniform=False)
        elif hparams.init_method == 'xavier_uniform':
            return tf.contrib.layers.xavier_initializer(uniform=True)
        elif hparams.init_method == 'he_normal':
            return tf.contrib.layers.variance_scaling_initializer( \
                factor=2.0, mode='FAN_IN', uniform=False)
        elif hparams.init_method == 'he_uniform':
            return tf.contrib.layers.variance_scaling_initializer( \
                factor=2.0, mode='FAN_IN', uniform=True)
        else:
            return tf.truncated_normal_initializer(stddev=hparams.init_value)

    def _compute_data_loss(self, hparams):
        if hparams.loss == 'cross_entropy_loss':
            data_loss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits( \
                logits=tf.reshape(self.logit, [-1]), \
                labels=tf.reshape(self.iterator.labels, [-1])))
        elif hparams.loss == 'square_loss':
            data_loss = tf.reduce_mean(tf.squared_difference(self.pred, self.iterator.labels))
        elif hparams.loss == 'log_loss':
            data_loss = tf.reduce_mean(tf.losses.log_loss(predictions=self.logit, labels=self.iterator.labels))
        else:
            raise ValueError("this loss not defined {0}".format(hparams.loss))
        return data_loss

    def _compute_regular_loss(self, hparams):
        regular_loss = self._l2_loss(hparams) + self._l1_loss(hparams)
        regular_loss = tf.reduce_sum(regular_loss)
        return regular_loss

    def _build_train_opt(self, hparams):
        def train_opt(hparams):
            if hparams.optimizer == 'adadelta':
                train_step = tf.train.AdadeltaOptimizer( \
                    hparams.learning_rate).minimize(self.loss)
            elif hparams.optimizer == 'adagrad':
                train_step = tf.train.AdagradOptimizer( \
                    hparams.learning_rate).minimize(self.loss)
            elif hparams.optimizer == 'sgd':
                train_step = tf.train.GradientDescentOptimizer( \
                    hparams.learning_rate).minimize(self.loss)
            elif hparams.optimizer == 'adam':
                train_step = tf.train.AdamOptimizer( \
                    hparams.learning_rate).minimize(self.loss)
            elif hparams.optimizer == 'ftrl':
                train_step = tf.train.FtrlOptimizer( \
                    hparams.learning_rate).minimize(self.loss)
            elif hparams.optimizer == 'gd':
                train_step = tf.train.GradientDescentOptimizer( \
                    hparams.learning_rate).minimize(self.loss)
            elif hparams.optimizer == 'padagrad':
                train_step = tf.train.ProximalAdagradOptimizer( \
                    hparams.learning_rate).minimize(self.loss)
            elif hparams.optimizer == 'pgd':
                train_step = tf.train.ProximalGradientDescentOptimizer( \
                    hparams.learning_rate).minimize(self.loss)
            elif hparams.optimizer == 'rmsprop':
                train_step = tf.train.RMSPropOptimizer( \
                    hparams.learning_rate).minimize(self.loss)
            else:
                train_step = tf.train.GradientDescentOptimizer( \
                    hparams.learning_rate).minimize(self.loss)
            return train_step

        train_step = train_opt(hparams)
        return train_step

    def _active_layer(self, logit, scope, activation):
        logit = self._activate(logit, activation)
        return logit

    def _activate(self, logit, activation):
        if activation == 'sigmoid':
            return tf.nn.sigmoid(logit)
        elif activation == 'softmax':
            return tf.nn.softmax(logit)
        elif activation == 'relu':
            return tf.nn.relu(logit)
        elif activation == 'tanh':
            return tf.nn.tanh(logit)
        elif activation == 'elu':
            return tf.nn.elu(logit)
        else:
            raise ValueError("this activations not defined {0}".format(activation))

    def train(self, sess):
        return sess.run([self.update, self.loss, \
                         self.data_loss, self.merged])

    def eval(self, sess):
        return sess.run([self.loss, self.data_loss, \
                         self.pred, self.iterator.labels])

    def infer(self, sess):
        return sess.run([self.pred])
