import sys
uid = set()
iid = set()
#TOU
cityid = set()
#TOI
domainid = set()

filename = './train.csv'
with open(filename, 'r') as f:
    for idx, line in enumerate(f):
        if idx==0:
            continue

        temps = line.strip().split(',')
        if len(temps) < 2:
            continue
        items = temps[1].strip().split('|')
        uid.add(int(items[1].strip().split('#')[1]))
        iid.add(int(items[2].strip().split('#')[1]))
        domainid.add(int(items[3].strip().split(':')[1]))
        cityid.add(int(items[4].strip().split(':')[1]))

print('min uid:', min(uid), 'max uid:', max(uid))
print('min iid:', min(iid), 'max iid:', max(iid))
print('min cityid:', min(cityid), 'max cityid:', max(cityid))
print('min domainid:', min(domainid), 'max domainid:', max(domainid))