from iterator import DhnIterator
import tensorflow as tf
import numpy as np
from dhn_cache import DhnCache
import sys


def get_uid(filename):
    uid_list = []
    for line in open(filename, encoding='utf8', mode='r'):
        if line.strip() is '':
            continue
        items = line.strip().split('')
        if len(items)==1:
            continue
        uid = items[-3]
        uid_list.append(uid)
    return uid_list

def infer_embedding(infer_path, infer_cached_path, model_path, res_path, hparams):
    # 在infer的时候，必须要使用dir(tf.contrib)这个操作。不然，模型是没办法将iterator等加载进来的
    dir(tf.contrib)
    #cache data
    cache_obj = DhnCache()
    cache_obj.write_tfrecord(infer_path, infer_cached_path, hparams)

    sess = tf.Session()
    meta_graph_def = tf.saved_model.loader.load(sess, ['tag_string'], model_path)
    #将code里面的几个迭代器给取出来.
    T2I_pair_feat_batch = sess.graph.get_tensor_by_name('T2I_pair_feat_batch:0')
    T2U_pair_feat_batch = sess.graph.get_tensor_by_name('T2U_pair_feat_batch:0')
    T2I_feat_batch = sess.graph.get_tensor_by_name('T2I_feat_batch:0')
    T2U_feat_batch = sess.graph.get_tensor_by_name('T2U_feat_batch:0')
    T1I_pair_feat_batch_indices = sess.graph.get_tensor_by_name('T1I_pair_feat_batch_indices:0')
    T1I_pair_feat_batch_val = sess.graph.get_tensor_by_name('T1I_pair_feat_batch_val:0')
    T1I_pair_feat_batch_weights = sess.graph.get_tensor_by_name('T1I_pair_feat_batch_weights:0')
    T1I_pair_feat_batch_shape = sess.graph.get_tensor_by_name('T1I_pair_feat_batch_shape:0')
    T1U_pair_feat_batch_indices = sess.graph.get_tensor_by_name('T1U_pair_feat_batch_indices:0')
    T1U_pair_feat_batch_val = sess.graph.get_tensor_by_name('T1U_pair_feat_batch_val:0')
    T1U_pair_feat_batch_weights = sess.graph.get_tensor_by_name('T1U_pair_feat_batch_weights:0')
    T1U_pair_feat_batch_shape = sess.graph.get_tensor_by_name('T1U_pair_feat_batch_shape:0')
    T0I_unique_feat_batch_indices = sess.graph.get_tensor_by_name('T0I_unique_feat_batch_indices:0')
    T0I_unique_feat_batch_val = sess.graph.get_tensor_by_name('T0I_unique_feat_batch_val:0')
    T0I_unique_feat_batch_weights = sess.graph.get_tensor_by_name('T0I_unique_feat_batch_weights:0')
    T0I_unique_feat_batch_shape = sess.graph.get_tensor_by_name('T0I_unique_feat_batch_shape:0')
    T0U_unique_feat_batch_indices = sess.graph.get_tensor_by_name('T0U_unique_feat_batch_indices:0')
    T0U_unique_feat_batch_val = sess.graph.get_tensor_by_name('T0U_unique_feat_batch_val:0')
    T0U_unique_feat_batch_weights = sess.graph.get_tensor_by_name('T0U_unique_feat_batch_weights:0')
    T0U_unique_feat_batch_shape = sess.graph.get_tensor_by_name('T0U_unique_feat_batch_shape:0')
    user_embedding = sess.graph.get_tensor_by_name('DhnModel/user_embedding:0')
    item_embedding = sess.graph.get_tensor_by_name('DhnModel/item_embedding:0')

    uid_list = get_uid(infer_path)
    uid_idx = 0
    print('get embedding uid num:', len(uid_list))
    out = open(res_path, 'w')
    filenames = [infer_cached_path]
    src_dataset = tf.contrib.data.TFRecordDataset(filenames)
    batch_iter = DhnIterator(src_dataset, hparams)

    sess.run(batch_iter.initializer)
    while True:
        try:
            res = sess.run([batch_iter.T2I_pair_feat_batch, batch_iter.T2U_pair_feat_batch, \
                            batch_iter.T2I_feat_batch, batch_iter.T2U_feat_batch, \

                            batch_iter.T1I_pair_feat_batch_indices, batch_iter.T1I_pair_feat_batch_val, \
                            batch_iter.T1I_pair_feat_batch_weights, batch_iter.T1I_pair_feat_batch_shape, \

                            batch_iter.T1U_pair_feat_batch_indices, batch_iter.T1U_pair_feat_batch_val, \
                            batch_iter.T1U_pair_feat_batch_weights, batch_iter.T1U_pair_feat_batch_shape, \

                            batch_iter.T0I_unique_feat_batch_indices, batch_iter.T0I_unique_feat_batch_val, \
                            batch_iter.T0I_unique_feat_batch_weights, batch_iter.T0I_unique_feat_batch_shape, \
    \
                            batch_iter.T0U_unique_feat_batch_indices, batch_iter.T0U_unique_feat_batch_val, \
                            batch_iter.T0U_unique_feat_batch_weights, batch_iter.T0U_unique_feat_batch_shape
                            ])
            user_embed = sess.run(user_embedding, feed_dict={
                T2I_pair_feat_batch : res[0],
                T2U_pair_feat_batch : res[1],
                T2I_feat_batch : res[2],
                T2U_feat_batch : res[3],
                T1I_pair_feat_batch_indices : res[4],
                T1I_pair_feat_batch_val : res[5],
                T1I_pair_feat_batch_weights : res[6],
                T1I_pair_feat_batch_shape : res[7],
                T1U_pair_feat_batch_indices : res[8],
                T1U_pair_feat_batch_val : res[9],
                T1U_pair_feat_batch_weights : res[10],
                T1U_pair_feat_batch_shape : res[11],
                T0I_unique_feat_batch_indices : res[12],
                T0I_unique_feat_batch_val : res[13],
                T0I_unique_feat_batch_weights : res[14],
                T0I_unique_feat_batch_shape : res[15],
                T0U_unique_feat_batch_indices : res[16],
                T0U_unique_feat_batch_val : res[17],
                T0U_unique_feat_batch_weights : res[18],
                T0U_unique_feat_batch_shape : res[19]
            })
            for one_embed in user_embed:
                uid = uid_list[uid_idx]
                uid_idx += 1
                line = uid + ':' + ','.join([str(val) for val in one_embed]) + '\n'
                out.write(line)

        except tf.errors.OutOfRangeError:
            print("end")
            break

def infer_embedding_from_session(infer_path, infer_cached_path, sess, res_path, hparams):
    # 在infer的时候，必须要使用dir(tf.contrib)这个操作。不然，模型是没办法将iterator等加载进来的
    dir(tf.contrib)
    #cache data
    cache_obj = DhnCache()
    cache_obj.write_tfrecord(infer_path, infer_cached_path, hparams)
    #将code里面的几个迭代器给取出来.
    T2I_pair_feat_batch = sess.graph.get_tensor_by_name('T2I_pair_feat_batch:0')
    T2U_pair_feat_batch = sess.graph.get_tensor_by_name('T2U_pair_feat_batch:0')
    T2I_feat_batch = sess.graph.get_tensor_by_name('T2I_feat_batch:0')
    T2U_feat_batch = sess.graph.get_tensor_by_name('T2U_feat_batch:0')
    T1I_pair_feat_batch_indices = sess.graph.get_tensor_by_name('T1I_pair_feat_batch_indices:0')
    T1I_pair_feat_batch_val = sess.graph.get_tensor_by_name('T1I_pair_feat_batch_val:0')
    T1I_pair_feat_batch_weights = sess.graph.get_tensor_by_name('T1I_pair_feat_batch_weights:0')
    T1I_pair_feat_batch_shape = sess.graph.get_tensor_by_name('T1I_pair_feat_batch_shape:0')
    T1U_pair_feat_batch_indices = sess.graph.get_tensor_by_name('T1U_pair_feat_batch_indices:0')
    T1U_pair_feat_batch_val = sess.graph.get_tensor_by_name('T1U_pair_feat_batch_val:0')
    T1U_pair_feat_batch_weights = sess.graph.get_tensor_by_name('T1U_pair_feat_batch_weights:0')
    T1U_pair_feat_batch_shape = sess.graph.get_tensor_by_name('T1U_pair_feat_batch_shape:0')
    T0I_unique_feat_batch_indices = sess.graph.get_tensor_by_name('T0I_unique_feat_batch_indices:0')
    T0I_unique_feat_batch_val = sess.graph.get_tensor_by_name('T0I_unique_feat_batch_val:0')
    T0I_unique_feat_batch_weights = sess.graph.get_tensor_by_name('T0I_unique_feat_batch_weights:0')
    T0I_unique_feat_batch_shape = sess.graph.get_tensor_by_name('T0I_unique_feat_batch_shape:0')
    T0U_unique_feat_batch_indices = sess.graph.get_tensor_by_name('T0U_unique_feat_batch_indices:0')
    T0U_unique_feat_batch_val = sess.graph.get_tensor_by_name('T0U_unique_feat_batch_val:0')
    T0U_unique_feat_batch_weights = sess.graph.get_tensor_by_name('T0U_unique_feat_batch_weights:0')
    T0U_unique_feat_batch_shape = sess.graph.get_tensor_by_name('T0U_unique_feat_batch_shape:0')
    user_embedding = sess.graph.get_tensor_by_name('DhnModel/user_embedding:0')
    item_embedding = sess.graph.get_tensor_by_name('DhnModel/item_embedding:0')

    uid_list = get_uid(infer_path)
    uid_idx = 0
    print('get embedding uid num:', len(uid_list))
    out = open(res_path, 'w')
    filenames = [infer_cached_path]
    src_dataset = tf.contrib.data.TFRecordDataset(filenames)
    batch_iter = DhnIterator(src_dataset, hparams)

    sess.run(batch_iter.initializer)
    while True:
        try:
            res = sess.run([batch_iter.T2I_pair_feat_batch, batch_iter.T2U_pair_feat_batch, \
                            batch_iter.T2I_feat_batch, batch_iter.T2U_feat_batch, \

                            batch_iter.T1I_pair_feat_batch_indices, batch_iter.T1I_pair_feat_batch_val, \
                            batch_iter.T1I_pair_feat_batch_weights, batch_iter.T1I_pair_feat_batch_shape, \

                            batch_iter.T1U_pair_feat_batch_indices, batch_iter.T1U_pair_feat_batch_val, \
                            batch_iter.T1U_pair_feat_batch_weights, batch_iter.T1U_pair_feat_batch_shape, \

                            batch_iter.T0I_unique_feat_batch_indices, batch_iter.T0I_unique_feat_batch_val, \
                            batch_iter.T0I_unique_feat_batch_weights, batch_iter.T0I_unique_feat_batch_shape, \
    \
                            batch_iter.T0U_unique_feat_batch_indices, batch_iter.T0U_unique_feat_batch_val, \
                            batch_iter.T0U_unique_feat_batch_weights, batch_iter.T0U_unique_feat_batch_shape
                            ])
            user_embed = sess.run(user_embedding, feed_dict={
                T2I_pair_feat_batch : res[0],
                T2U_pair_feat_batch : res[1],
                T2I_feat_batch : res[2],
                T2U_feat_batch : res[3],
                T1I_pair_feat_batch_indices : res[4],
                T1I_pair_feat_batch_val : res[5],
                T1I_pair_feat_batch_weights : res[6],
                T1I_pair_feat_batch_shape : res[7],
                T1U_pair_feat_batch_indices : res[8],
                T1U_pair_feat_batch_val : res[9],
                T1U_pair_feat_batch_weights : res[10],
                T1U_pair_feat_batch_shape : res[11],
                T0I_unique_feat_batch_indices : res[12],
                T0I_unique_feat_batch_val : res[13],
                T0I_unique_feat_batch_weights : res[14],
                T0I_unique_feat_batch_shape : res[15],
                T0U_unique_feat_batch_indices : res[16],
                T0U_unique_feat_batch_val : res[17],
                T0U_unique_feat_batch_weights : res[18],
                T0U_unique_feat_batch_shape : res[19]
            })
            for one_embed in user_embed:
                uid = uid_list[uid_idx]
                uid_idx += 1
                line = uid + ':' + ','.join([str(val) for val in one_embed]) + '\n'
                out.write(line)

        except tf.errors.OutOfRangeError:
            print("end")
            break


config = {
    'T2U_feat_len': 1,
    'T2I_feat_len': 1,
    'T2_pair_feat_num': 1,
    'T1_pair_field_num': 2,
    'T1_pair_feat_num': 156125,
    'T0U_unique_field_num': 2,
    'T0U_unique_feat_num': 32766,
    'T0I_unique_field_num': 1,
    'T0I_unique_feat_num': 2,
    'model_type': 'Dhn',
    'layer_sizes': [100, 100],
    'activation': ['relu', 'relu'],
    'dim': 10,
    'init_method': 'tnormal',
    'init_value': 0.001,
    'embed_l2': 0.0,
    'embed_l1': 0.0,
    'layer_l2': 0.0,
    'layer_l1': 0.0,
    'learning_rate': 0.0001,
    'loss': 'square_loss',
    'optimizer': 'adam',
    'epochs': 2,
    'batch_size': 32,
    'show_step': 10,
    'save_epoch': 1
}

def create_hparams(FLAGS):
    """Create hparams."""
    return tf.contrib.training.HParams(
        # data
        train_file=FLAGS['train_file'] if 'train_file' in FLAGS else None,
        eval_file=FLAGS['eval_file'] if 'eval_file' in FLAGS else None,
        T2U_feat_len=FLAGS['T2U_feat_len'] if 'T2U_feat_len' in FLAGS else None,
        T2I_feat_len=FLAGS['T2I_feat_len'] if 'T2I_feat_len' in FLAGS else None,
        T2_pair_feat_num=FLAGS['T2_pair_feat_num'] if 'T2_pair_feat_num' in FLAGS else None,
        T1_pair_field_num=FLAGS['T1_pair_field_num'] if 'T1_pair_field_num' in FLAGS else None,
        T1_pair_feat_num=FLAGS['T1_pair_feat_num'] if 'T1_pair_feat_num' in FLAGS else None,
        T0U_unique_field_num=FLAGS['T0U_unique_field_num'] if 'T0U_unique_field_num' in FLAGS else None,
        T0U_unique_feat_num=FLAGS['T0U_unique_feat_num'] if 'T0U_unique_feat_num' in FLAGS else None,
        T0I_unique_field_num=FLAGS['T0I_unique_field_num'] if 'T0I_unique_field_num' in FLAGS else None,
        T0I_unique_feat_num=FLAGS['T0I_unique_feat_num'] if 'T0I_unique_feat_num' in FLAGS else None,

        # model
        model_type=FLAGS['model_type'] if 'model_type' in FLAGS else None,
        layer_sizes=FLAGS['layer_sizes'] if 'layer_sizes' in FLAGS else None,
        activation=FLAGS['activation'] if 'activation' in FLAGS else None,
        load_model_name=FLAGS['load_model_name'] if 'load_model_name' in FLAGS else None,
        dim=FLAGS['dim'] if 'dim' in FLAGS else None,

        # train
        init_method=FLAGS['init_method'] if 'init_method' in FLAGS else 'tnormal',
        init_value=FLAGS['init_value'] if 'init_value' in FLAGS else 0.01,
        embed_l2=FLAGS['embed_l2'] if 'embed_l2' in FLAGS else 0.0000,
        embed_l1=FLAGS['embed_l1'] if 'embed_l1' in FLAGS else 0.0000,
        layer_l2=FLAGS['layer_l2'] if 'layer_l2' in FLAGS else 0.0000,
        layer_l1=FLAGS['layer_l1'] if 'layer_l1' in FLAGS else 0.0000,
        learning_rate=FLAGS['learning_rate'] if 'learning_rate' in FLAGS else 0.001,
        loss=FLAGS['loss'] if 'loss' in FLAGS else None,
        optimizer=FLAGS['optimizer'] if 'optimizer' in FLAGS else 'adam',
        epochs=FLAGS['epochs'] if 'epochs' in FLAGS else 10,
        batch_size=FLAGS['batch_size'] if 'batch_size' in FLAGS else 1,

        # show info
        show_step=FLAGS['show_step'] if 'show_step' in FLAGS else 1,
        metrics=FLAGS['metrics'] if 'metrics' in FLAGS else None,
        save_epoch=FLAGS['save_epoch'] if 'save_epoch' in FLAGS else None,

        # save model
        model_dir=FLAGS['model_dir'] if 'model_dir' in FLAGS else None,
        cache_path=FLAGS['cache_path'] if 'cache_path' in FLAGS else None,
    )

#hparams = create_hparams(config)
#infer_path = '../data/user_embedding_input.txt'
#infer_cached_path = '../data/infer_embedding.tfrecord'
#model_path = '../data/model/2017-10-25_17_00_13_epoch_2'
#res_path = '../data/emebdding_res.txt'
#infer_embedding(infer_path, infer_cached_path, model_path, res_path, hparams)