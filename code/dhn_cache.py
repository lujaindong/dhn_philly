"""define deep interest network cache class for reading data"""
from base_cache import BaseCache
import tensorflow as tf
import numpy as np
from collections import defaultdict
import util as util
import sys

__all__ = ["DhnCache"]


# field index start by 1, feat index start by 1
class DhnCache(BaseCache):
    def _load_batch_data_from_file(self, file, hparams):
        batch_size = hparams.batch_size
        labels = []
        T2I_pair_feat_batch = []
        T2U_pair_feat_batch = []
        T2I_feat_batch = []
        T2U_feat_batch = []
        T1I_pair_feat_batch = []
        T1U_pair_feat_batch = []
        T0I_unique_feat_batch = []
        T0U_unique_feat_batch = []
        cnt = 0
        total = 0
        with open(file, encoding='utf8') as f:
            for line in f:
                total += 1
                if line.strip() is '':
                    continue
                cols = line.strip().split('\x03')
                if len(cols) < 9:
                    continue
                label = cols[0]
                if float(label) > 0:
                    label = 1
                else:
                    label = 0
                # process T2U_feat
                if cols[1].strip() is '':
                    T2U_feat = [0.0]
                else:
                    T2U_feat = cols[1].strip().split(',')
                    T2U_feat = [float(val) for val in T2U_feat]
                # process T2I_feat
                if cols[2].strip() is '':
                    T2I_feat = [0.0]
                else:
                    T2I_feat = cols[2].strip().split(',')
                    T2I_feat = [float(val) for val in T2I_feat]
                # process T2U_pair_feat
                if cols[3].strip() is '':
                    feat_embedding_user_cascade = [0.0]
                else:
                    pair_feat_items = cols[3].strip().split(':')
                    if len(pair_feat_items) > 0:
                        feat_embedding_user_cascade = []
                        for i in range(len(pair_feat_items)):
                            if i % 2 == 0:
                                pass
                            else:
                                feat_embedding = [float(val) for val in pair_feat_items[i].strip().split(',')]
                                feat_embedding_user_cascade.extend(feat_embedding)
                # process T2I_pair_feat
                if cols[4].strip() is '':
                    feat_embedding_item_cascade = [0.0]
                else:
                    pair_feat_items = cols[4].strip().split(':')
                    feat_embedding_item_cascade = []
                    for i in range(len(pair_feat_items)):
                        if i % 2 == 0:
                            pass
                        else:
                            feat_embedding = [float(val) for val in pair_feat_items[i].strip().split(',')]
                            feat_embedding_item_cascade.extend(feat_embedding)

                # process T1U_pair_feat
                curr_T1U_pair_feat = []
                for field in range(hparams.T1_pair_field_num):
                    curr_T1U_pair_feat.append([field, 0, 0])
                if cols[5].strip() is '':
                    pass
                else:
                    terms = cols[5].strip().split(',')
                    for word in terms:
                        tokens = word.strip().split(':')
                        curr_T1U_pair_feat.append([int(tokens[0]) - 1, int(tokens[1]), float(tokens[2])])

                # process T1I_pair_feat
                curr_T1I_pair_feat = []
                for field in range(hparams.T1_pair_field_num):
                    curr_T1I_pair_feat.append([field, 0, 0])
                if cols[6].strip() is '':
                    pass
                else:
                    terms = cols[6].strip().split(',')
                    for word in terms:
                        tokens = word.strip().split(':')
                        curr_T1I_pair_feat.append([int(tokens[0]) - 1, int(tokens[1]), float(tokens[2])])

                # process T0U_unique_feat
                curr_T0U_unique_feat = []
                # 保证每个field都有一个feat, 因此对每个field都填充一个field_index:0:0
                for field in range(hparams.T0U_unique_field_num):
                    curr_T0U_unique_feat.append([field, 0, 0])
                if cols[7].strip() is '':
                    pass
                else:
                    terms = cols[7].strip().split(',')
                    for word in terms:
                        tokens = word.strip().split(':')
                        curr_T0U_unique_feat.append([int(tokens[0]) - 1, int(tokens[1]), float(tokens[2])])

                # process T0I_unique_feat
                curr_T0I_unique_feat = []
                for field in range(hparams.T0I_unique_field_num):
                    curr_T0I_unique_feat.append([field, 0, 0])
                if cols[8].strip() is '':
                    pass
                else:
                    terms = cols[8].strip().split(',')
                    for word in terms:
                        tokens = word.strip().split(':')
                        curr_T0I_unique_feat.append([int(tokens[0]) - 1, int(tokens[1]), float(tokens[2])])
                cnt += 1
                labels.append([label])
                T2I_pair_feat_batch.append(feat_embedding_item_cascade)
                T2U_pair_feat_batch.append(feat_embedding_user_cascade)
                T2I_feat_batch.append(T2I_feat)
                T2U_feat_batch.append(T2U_feat)
                T1I_pair_feat_batch.append(curr_T1I_pair_feat)
                T1U_pair_feat_batch.append(curr_T1U_pair_feat)
                T0U_unique_feat_batch.append(curr_T0U_unique_feat)
                T0I_unique_feat_batch.append(curr_T0I_unique_feat)
                if total % 100000 == 0:
                    print('cache sample num:', total)
                if cnt == batch_size:
                    yield labels, T2U_feat_batch, T2I_feat_batch, T2U_pair_feat_batch, T2I_pair_feat_batch, T1U_pair_feat_batch, \
                          T1I_pair_feat_batch, T0U_unique_feat_batch, T0I_unique_feat_batch
                    labels = []
                    T2U_feat_batch = []
                    T2I_feat_batch = []
                    T2U_pair_feat_batch = []
                    T2I_pair_feat_batch = []
                    T1U_pair_feat_batch = []
                    T1I_pair_feat_batch = []
                    T0U_unique_feat_batch = []
                    T0I_unique_feat_batch = []
                    cnt = 0
            if cnt > 0:
                yield labels, T2U_feat_batch, T2I_feat_batch, T2U_pair_feat_batch, T2I_pair_feat_batch, T1U_pair_feat_batch, \
                      T1I_pair_feat_batch, T0U_unique_feat_batch, T0I_unique_feat_batch

    def _convert_data(self, labels, T2U_feat_batch, T2I_feat_batch, T2U_pair_feat_batch, T2I_pair_feat_batch,
                      T1U_pair_feat_batch, \
                      T1I_pair_feat_batch, T0U_unique_feat_batch, T0I_unique_feat_batch, hparams):
        batch_size = hparams.batch_size
        instance_cnt = len(labels)

        T1I_pair_feat_batch_indices = []
        T1I_pair_feat_batch_val = []
        T1I_pair_feat_batch_weights = []
        T1I_pair_feat_batch_shape = [instance_cnt * hparams.T1_pair_field_num, -1]
        for i in range(instance_cnt):
            m = len(T1I_pair_feat_batch[i])
            dnn_feat_dic = {}
            for j in range(m):
                if T1I_pair_feat_batch[i][j][0] not in dnn_feat_dic:
                    dnn_feat_dic[T1I_pair_feat_batch[i][j][0]] = 0
                else:
                    dnn_feat_dic[T1I_pair_feat_batch[i][j][0]] += 1
                T1I_pair_feat_batch_indices.append([i * hparams.T1_pair_field_num + T1I_pair_feat_batch[i][j][0], \
                                                    dnn_feat_dic[T1I_pair_feat_batch[i][j][0]]])
                T1I_pair_feat_batch_val.append(T1I_pair_feat_batch[i][j][1])
                T1I_pair_feat_batch_weights.append(T1I_pair_feat_batch[i][j][2])
                if T1I_pair_feat_batch_shape[1] < dnn_feat_dic[T1I_pair_feat_batch[i][j][0]]:
                    T1I_pair_feat_batch_shape[1] = dnn_feat_dic[T1I_pair_feat_batch[i][j][0]]
        T1I_pair_feat_batch_shape[1] += 1

        T1U_pair_feat_batch_indices = []
        T1U_pair_feat_batch_val = []
        T1U_pair_feat_batch_weights = []
        T1U_pair_feat_batch_shape = [instance_cnt * hparams.T1_pair_field_num, -1]
        for i in range(instance_cnt):
            m = len(T1U_pair_feat_batch[i])
            dnn_feat_dic = {}
            for j in range(m):
                if T1U_pair_feat_batch[i][j][0] not in dnn_feat_dic:
                    dnn_feat_dic[T1U_pair_feat_batch[i][j][0]] = 0
                else:
                    dnn_feat_dic[T1U_pair_feat_batch[i][j][0]] += 1
                T1U_pair_feat_batch_indices.append([i * hparams.T1_pair_field_num + T1U_pair_feat_batch[i][j][0], \
                                                    dnn_feat_dic[T1U_pair_feat_batch[i][j][0]]])
                T1U_pair_feat_batch_val.append(T1U_pair_feat_batch[i][j][1])
                T1U_pair_feat_batch_weights.append(T1U_pair_feat_batch[i][j][2])
                if T1U_pair_feat_batch_shape[1] < dnn_feat_dic[T1U_pair_feat_batch[i][j][0]]:
                    T1U_pair_feat_batch_shape[1] = dnn_feat_dic[T1U_pair_feat_batch[i][j][0]]
        T1U_pair_feat_batch_shape[1] += 1

        T0U_unique_feat_batch_indices = []
        T0U_unique_feat_batch_val = []
        T0U_unique_feat_batch_weights = []
        T0U_unique_feat_batch_shape = [instance_cnt * hparams.T0U_unique_field_num, -1]
        for i in range(instance_cnt):
            m = len(T0U_unique_feat_batch[i])
            dnn_feat_dic = {}
            for j in range(m):
                if T0U_unique_feat_batch[i][j][0] not in dnn_feat_dic:
                    dnn_feat_dic[T0U_unique_feat_batch[i][j][0]] = 0
                else:
                    dnn_feat_dic[T0U_unique_feat_batch[i][j][0]] += 1
                T0U_unique_feat_batch_indices.append([i * hparams.T0U_unique_field_num + T0U_unique_feat_batch[i][j][0], \
                                                      dnn_feat_dic[T0U_unique_feat_batch[i][j][0]]])
                T0U_unique_feat_batch_val.append(T0U_unique_feat_batch[i][j][1])
                T0U_unique_feat_batch_weights.append(T0U_unique_feat_batch[i][j][2])
                if T0U_unique_feat_batch_shape[1] < dnn_feat_dic[T0U_unique_feat_batch[i][j][0]]:
                    T0U_unique_feat_batch_shape[1] = dnn_feat_dic[T0U_unique_feat_batch[i][j][0]]
        T0U_unique_feat_batch_shape[1] += 1

        T0I_unique_feat_batch_indices = []
        T0I_unique_feat_batch_val = []
        T0I_unique_feat_batch_weights = []
        T0I_unique_feat_batch_shape = [instance_cnt * hparams.T0I_unique_field_num, -1]
        for i in range(instance_cnt):
            m = len(T0I_unique_feat_batch[i])
            dnn_feat_dic = {}
            for j in range(m):
                if T0I_unique_feat_batch[i][j][0] not in dnn_feat_dic:
                    dnn_feat_dic[T0I_unique_feat_batch[i][j][0]] = 0
                else:
                    dnn_feat_dic[T0I_unique_feat_batch[i][j][0]] += 1
                T0I_unique_feat_batch_indices.append([i * hparams.T0I_unique_field_num + T0I_unique_feat_batch[i][j][0], \
                                                      dnn_feat_dic[T0I_unique_feat_batch[i][j][0]]])
                T0I_unique_feat_batch_val.append(T0I_unique_feat_batch[i][j][1])
                T0I_unique_feat_batch_weights.append(T0I_unique_feat_batch[i][j][2])
                if T0I_unique_feat_batch_shape[1] < dnn_feat_dic[T0I_unique_feat_batch[i][j][0]]:
                    T0I_unique_feat_batch_shape[1] = dnn_feat_dic[T0I_unique_feat_batch[i][j][0]]
        T0I_unique_feat_batch_shape[1] += 1

        res = {}
        res['labels'] = np.asarray(labels, dtype=np.float32)
        res['T2I_feat_batch'] = np.asarray(T2I_feat_batch, dtype=np.float32)
        res['T2U_feat_batch'] = np.asarray(T2U_feat_batch, dtype=np.float32)
        res['T2I_pair_feat_batch'] = np.asarray(T2I_pair_feat_batch, dtype=np.float32)
        res['T2U_pair_feat_batch'] = np.asarray(T2U_pair_feat_batch, dtype=np.float32)

        sorted_T1I_indices = sorted(range(len(T1I_pair_feat_batch_indices)),
                                    key=lambda k: (T1I_pair_feat_batch_indices[k][0], \
                                                   T1I_pair_feat_batch_indices[k][1]))
        res['T1I_pair_feat_batch_indices'] = np.asarray(T1I_pair_feat_batch_indices, dtype=np.int64)[
            sorted_T1I_indices]
        res['T1I_pair_feat_batch_val'] = np.asarray(T1I_pair_feat_batch_val, dtype=np.int64)[sorted_T1I_indices]
        res['T1I_pair_feat_batch_weights'] = np.asarray(T1I_pair_feat_batch_weights, dtype=np.float32)[
            sorted_T1I_indices]
        res['T1I_pair_feat_batch_shape'] = np.asarray(T1I_pair_feat_batch_shape, dtype=np.int64)

        sorted_T1U_indices = sorted(range(len(T1U_pair_feat_batch_indices)),
                                    key=lambda k: (T1U_pair_feat_batch_indices[k][0], \
                                                   T1U_pair_feat_batch_indices[k][1]))
        res['T1U_pair_feat_batch_indices'] = np.asarray(T1U_pair_feat_batch_indices, dtype=np.int64)[
            sorted_T1U_indices]
        res['T1U_pair_feat_batch_val'] = np.asarray(T1U_pair_feat_batch_val, dtype=np.int64)[sorted_T1U_indices]
        res['T1U_pair_feat_batch_weights'] = np.asarray(T1U_pair_feat_batch_weights, dtype=np.float32)[
            sorted_T1U_indices]
        res['T1U_pair_feat_batch_shape'] = np.asarray(T1U_pair_feat_batch_shape, dtype=np.int64)

        sorted_T0I_indices = sorted(range(len(T0I_unique_feat_batch_indices)),
                                    key=lambda k: (T0I_unique_feat_batch_indices[k][0], \
                                                   T0I_unique_feat_batch_indices[k][1]))
        res['T0I_unique_feat_batch_indices'] = np.asarray(T0I_unique_feat_batch_indices, dtype=np.int64)[
            sorted_T0I_indices]
        res['T0I_unique_feat_batch_val'] = np.asarray(T0I_unique_feat_batch_val, dtype=np.int64)[sorted_T0I_indices]
        res['T0I_unique_feat_batch_weights'] = np.asarray(T0I_unique_feat_batch_weights, dtype=np.float32)[
            sorted_T0I_indices]
        res['T0I_unique_feat_batch_shape'] = np.asarray(T0I_unique_feat_batch_shape, dtype=np.int64)

        sorted_T0U_indices = sorted(range(len(T0U_unique_feat_batch_indices)),
                                    key=lambda k: (T0U_unique_feat_batch_indices[k][0], \
                                                   T0U_unique_feat_batch_indices[k][1]))
        res['T0U_unique_feat_batch_indices'] = np.asarray(T0U_unique_feat_batch_indices, dtype=np.int64)[
            sorted_T0U_indices]
        res['T0U_unique_feat_batch_val'] = np.asarray(T0U_unique_feat_batch_val, dtype=np.int64)[sorted_T0U_indices]
        res['T0U_unique_feat_batch_weights'] = np.asarray(T0U_unique_feat_batch_weights, dtype=np.float32)[
            sorted_T0U_indices]
        res['T0U_unique_feat_batch_shape'] = np.asarray(T0U_unique_feat_batch_shape, dtype=np.int64)

        return res

    def write_tfrecord(self, infile, outfile, hparams):
        writer = tf.python_io.TFRecordWriter(outfile)
        for index, (
                labels, T2U_feat_batch, T2I_feat_batch, T2U_pair_feat_batch, T2I_pair_feat_batch, T1U_pair_feat_batch, \
                T1I_pair_feat_batch, T0U_unique_feat_batch, T0I_unique_feat_batch) in enumerate(
            self._load_batch_data_from_file(infile, hparams)):
            input_in_sp = self._convert_data(labels, T2U_feat_batch, T2I_feat_batch, T2U_pair_feat_batch,
                                             T2I_pair_feat_batch, T1U_pair_feat_batch, \
                                             T1I_pair_feat_batch, T0U_unique_feat_batch, T0I_unique_feat_batch, hparams)

            labels = input_in_sp['labels']
            T2I_feat_batch = input_in_sp['T2I_feat_batch']
            T2U_feat_batch = input_in_sp['T2U_feat_batch']

            T2I_pair_feat_batch = input_in_sp['T2I_pair_feat_batch']
            T2U_pair_feat_batch = input_in_sp['T2U_pair_feat_batch']

            T1I_pair_feat_batch_indices = input_in_sp['T1I_pair_feat_batch_indices']
            T1I_pair_feat_batch_val = input_in_sp['T1I_pair_feat_batch_val']
            T1I_pair_feat_batch_weights = input_in_sp['T1I_pair_feat_batch_weights']
            T1I_pair_feat_batch_shape = input_in_sp['T1I_pair_feat_batch_shape']

            T1U_pair_feat_batch_indices = input_in_sp['T1U_pair_feat_batch_indices']
            T1U_pair_feat_batch_val = input_in_sp['T1U_pair_feat_batch_val']
            T1U_pair_feat_batch_weights = input_in_sp['T1U_pair_feat_batch_weights']
            T1U_pair_feat_batch_shape = input_in_sp['T1U_pair_feat_batch_shape']

            T0I_unique_feat_batch_indices = input_in_sp['T0I_unique_feat_batch_indices']
            T0I_unique_feat_batch_val = input_in_sp['T0I_unique_feat_batch_val']
            T0I_unique_feat_batch_weights = input_in_sp['T0I_unique_feat_batch_weights']
            T0I_unique_feat_batch_shape = input_in_sp['T0I_unique_feat_batch_shape']

            T0U_unique_feat_batch_indices = input_in_sp['T0U_unique_feat_batch_indices']
            T0U_unique_feat_batch_val = input_in_sp['T0U_unique_feat_batch_val']
            T0U_unique_feat_batch_weights = input_in_sp['T0U_unique_feat_batch_weights']
            T0U_unique_feat_batch_shape = input_in_sp['T0U_unique_feat_batch_shape']

            labels_str = labels.tostring()
            T2I_feat_batch_str = T2I_feat_batch.tostring()
            T2U_feat_batch_str = T2U_feat_batch.tostring()
            T2I_pair_feat_batch_str = T2I_pair_feat_batch.tostring()
            T2U_pair_feat_batch_str = T2U_pair_feat_batch.tostring()

            T1I_pair_feat_batch_indices_str = T1I_pair_feat_batch_indices.tostring()
            T1U_pair_feat_batch_indices_str = T1U_pair_feat_batch_indices.tostring()
            T0I_unique_feat_batch_indices_str = T0I_unique_feat_batch_indices.tostring()
            T0U_unique_feat_batch_indices_str = T0U_unique_feat_batch_indices.tostring()

            example = tf.train.Example(
                features=tf.train.Features(
                    feature={
                        'T2I_feat_batch': tf.train.Feature(
                            bytes_list=tf.train.BytesList(value=[T2I_feat_batch_str])),
                        'T2U_feat_batch': tf.train.Feature(
                            bytes_list=tf.train.BytesList(value=[T2U_feat_batch_str])),
                        'T2I_pair_feat_batch': tf.train.Feature(
                            bytes_list=tf.train.BytesList(value=[T2I_pair_feat_batch_str])),
                        'T2U_pair_feat_batch': tf.train.Feature(
                            bytes_list=tf.train.BytesList(value=[T2U_pair_feat_batch_str])),
                        'labels': tf.train.Feature(
                            bytes_list=tf.train.BytesList(value=[labels_str])),

                        'T1I_pair_feat_batch_indices': tf.train.Feature(
                            bytes_list=tf.train.BytesList(value=[T1I_pair_feat_batch_indices_str])),
                        'T1I_pair_feat_batch_val': tf.train.Feature(
                            int64_list=tf.train.Int64List(value=T1I_pair_feat_batch_val)),
                        'T1I_pair_feat_batch_weights': tf.train.Feature(
                            float_list=tf.train.FloatList(value=T1I_pair_feat_batch_weights)),
                        'T1I_pair_feat_batch_shape': tf.train.Feature(
                            int64_list=tf.train.Int64List(value=T1I_pair_feat_batch_shape)),

                        'T1U_pair_feat_batch_indices': tf.train.Feature(
                            bytes_list=tf.train.BytesList(value=[T1U_pair_feat_batch_indices_str])),
                        'T1U_pair_feat_batch_val': tf.train.Feature(
                            int64_list=tf.train.Int64List(value=T1U_pair_feat_batch_val)),
                        'T1U_pair_feat_batch_weights': tf.train.Feature(
                            float_list=tf.train.FloatList(value=T1U_pair_feat_batch_weights)),
                        'T1U_pair_feat_batch_shape': tf.train.Feature(
                            int64_list=tf.train.Int64List(value=T1U_pair_feat_batch_shape)),

                        'T0U_unique_feat_batch_indices': tf.train.Feature(
                            bytes_list=tf.train.BytesList(value=[T0U_unique_feat_batch_indices_str])),
                        'T0U_unique_feat_batch_val': tf.train.Feature(
                            int64_list=tf.train.Int64List(value=T0U_unique_feat_batch_val)),
                        'T0U_unique_feat_batch_weights': tf.train.Feature(
                            float_list=tf.train.FloatList(value=T0U_unique_feat_batch_weights)),
                        'T0U_unique_feat_batch_shape': tf.train.Feature(
                            int64_list=tf.train.Int64List(value=T0U_unique_feat_batch_shape)),

                        'T0I_unique_feat_batch_indices': tf.train.Feature(
                            bytes_list=tf.train.BytesList(value=[T0I_unique_feat_batch_indices_str])),
                        'T0I_unique_feat_batch_val': tf.train.Feature(
                            int64_list=tf.train.Int64List(value=T0I_unique_feat_batch_val)),
                        'T0I_unique_feat_batch_weights': tf.train.Feature(
                            float_list=tf.train.FloatList(value=T0I_unique_feat_batch_weights)),
                        'T0I_unique_feat_batch_shape': tf.train.Feature(
                            int64_list=tf.train.Int64List(value=T0I_unique_feat_batch_shape))
                    }
                )
            )
            serialized = example.SerializeToString()
            writer.write(serialized)


def test_cache():
    def create_hparams():
        """Create hparams."""
        return tf.contrib.training.HParams(
            batch_size=3,
            T2U_feat_len=5,
            T2I_feat_len=2,
            T2_pair_feat_num=10,
            T1_pair_field_num=3,
            T1_pair_feat_num=100,
            T0U_unique_field_num=4,
            T0U_unique_feat_num=200,
            T0I_unique_field_num=5,
            T0I_unique_feat_num=300
        )

    hparams = create_hparams()
    infile = './train_dhn_final.csv'
    outfile = './train.tfrecord'
    cache = DhnCache()
    cache.write_tfrecord(infile, outfile, hparams)
    print('end')

# test_cache()
